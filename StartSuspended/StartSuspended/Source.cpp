#include <windows.h>
#include <stdio.h>
#include <iostream>

using std::string;

int main()
{
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	const char* to_run = "to_run.exe";

	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));

	CreateProcess(NULL,   // No module name (use command line)
		const_cast<LPSTR>(to_run),        // Command line
		NULL,           // Process handle not inheritable
		NULL,           // Thread handle not inheritable
		FALSE,          // Set handle inheritance to FALSE
		CREATE_SUSPENDED,              // Create Suspended
		NULL,           // Use parent's environment block
		NULL,           // Use parent's starting directory 
		&si,            // Pointer to STARTUPINFO structure
		&pi);           // Pointer to PROCESS_INFORMATION structure

	system(string("Dllinjector to_run.exe hookdll.dll").c_str());
	Sleep(500);
	ResumeThread(pi.hThread);
	return 0;
}