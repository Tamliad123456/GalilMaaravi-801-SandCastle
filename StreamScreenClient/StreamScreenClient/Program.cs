using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Net.Sockets;
using System.Net;
using System.Diagnostics;
using System.Windows.Forms;
using WindowsInput.Native;
using WindowsInput;

namespace StreamScreenClient
{
    class Program
    {
        private static readonly int BUFFER_SIZE = 1024;
        [DllImport("user32.dll")]
        static extern bool SetCursorPos(int X, int Y);

        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern void mouse_event(uint dwFlags, uint dx, uint dy, uint cButtons, uint dwExtraInfo);
        //Mouse actions
        private const int MOUSEEVENTF_LEFTDOWN = 0x02;
        private const int MOUSEEVENTF_LEFTUP = 0x04;
        private const int MOUSEEVENTF_RIGHTDOWN = 0x08;
        private const int MOUSEEVENTF_RIGHTUP = 0x10;

        public enum MouseEventFlags
        {
            LeftDown = 0x00000002,
            LeftUp = 0x00000004,
            MiddleDown = 0x00000020,
            MiddleUp = 0x00000040,
            Move = 0x00000001,
            Absolute = 0x00008000,
            RightDown = 0x00000008,
            RightUp = 0x00000010
        }


        private static readonly string IP = "0.0.0.0";

        static void Main(string[] args)
        {
            //Console.WriteLine("Hello World!");
            openSocketAndConnect();
            Console.ReadKey();

        }

        private static void openSocketAndConnect()
        {
            IPAddress ipAddress = IPAddress.Parse(IP);
            IPEndPoint localEndPoint = new IPEndPoint(ipAddress, 5858);

            Socket listener = new Socket(ipAddress.AddressFamily,
                        SocketType.Stream, ProtocolType.Tcp);
            try
            {
                listener.Bind(localEndPoint);
                Console.WriteLine("here");
                listener.Listen(1);

                //Process p = new Process();
                //p.StartInfo.FileName = "dothestream.exe";
                //p.Start();

                while (true)
                {
                    Socket handler = listener.Accept();
                    Console.WriteLine("connected");
                    while(true)
                    {
                        doAction(handler);
                    }
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

            Console.WriteLine("\nPress ENTER to continue...");
            Console.Read();
        }

        private static void doAction(Socket s)
        {
            try
            {
                byte[] byteArr = new byte[BUFFER_SIZE];
                s.Receive(byteArr);
                string str = Encoding.ASCII.GetString(byteArr);
                Console.WriteLine(str);
                if(str[0] == '1')
                {
                    int x = Int32.Parse(str.Split(',')[1]);
                    int y = Int32.Parse(str.Split(',')[2]);
                    int type = Int32.Parse(str.Split(',')[3]);
                    SetCursorPos(x, y);
                    if (type == 0)
                        mouse_event((int)MouseEventFlags.LeftDown | (int)MouseEventFlags.LeftUp, 0, 0, 0, 0);
                    else
                        mouse_event((int)MouseEventFlags.RightDown | (int)MouseEventFlags.RightUp, 0, 0, 0, 0);

                }
                else if(str[0] == '0')
                {
                    string key = str.Split(',')[1];
                    string newKey = "";
                    if(key.Length > 1)
                    {
                        key = key.ToUpper();
                    }
                    for( int i = 0; i < key.Length; i++)
                    {
                        if(key[i] >= 'A' && key[i] <= 'Z' || key[i] == ' ')
                        {
                            newKey += key[i].ToString();
                        }
                    }
                    if (newKey != " ")
                    {
                        SendKeys.SendWait("{" + newKey + "}");
                    }
                    else
                    {
                        SendKeys.SendWait(newKey);
                    }
                    SendKeys.Flush();
                }

            }
            catch(Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        [DllImport("User32.dll")]
        static extern int SetForegroundWindow(IntPtr point);

        static void keyHook()
        {
            Socket s = openSocket();
            Console.WriteLine("accepted");
            IntPtr pointer = IntPtr.Zero; ;

            Process[] p = Process.GetProcessesByName("to_run.exe");//we can get it by handle also
            foreach (var phndl in p)
            {
                Console.WriteLine("foreach");
                pointer = phndl.Handle;
            }
            SetForegroundWindow(pointer);

            while (true)
            {
                recvData(s);
            }

        }

        private static Socket openSocket()
        {
            IPAddress ipAddress = IPAddress.Parse("0.0.0.0");
            IPEndPoint localEndPoint = new IPEndPoint(ipAddress, 5858);

            Socket listener = new Socket(ipAddress.AddressFamily,
                        SocketType.Stream, ProtocolType.Tcp);
            listener.Bind(localEndPoint);
            listener.Listen(1);
            Console.WriteLine("here");
            return listener.Accept();
        }

        private static string recvData(Socket s)
        {
            byte[] buff = new byte[2048];
            s.Receive(buff);

            Console.WriteLine("HERE");
            Console.WriteLine(Encoding.ASCII.GetString(buff));

            return Encoding.ASCII.GetString(buff);
        }
    }
}
