﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SandCastleGui
{
    public partial class Settings : Form
    {
        public Settings()
        {
            InitializeComponent();

            string[] settings = System.IO.File.ReadAllLines(@"config.cfg");

            foreach (string setting in settings)
            {
                string[] arr = setting.Split('=');
                switch (arr[0])
                {
                    case "Dynamic":
                        if (arr[1] == "True")
                        {
                            DynamicBtn.Checked = true;
                        }
                        else if (arr[1] == "False")
                        {
                            DynamicBtn.Checked = false;
                        }
                        break;
                    case "Static":
                        if (arr[1] == "True")
                        {
                            StaticBtn.Checked = true;
                        }
                        else if (arr[1] == "False")
                        {
                            StaticBtn.Checked = false;
                        }
                        break;
                    case "Snapshot":
                        SnapshotTextBox.Text = arr[1];
                        break;
                    case "VM_Name":
                        VMNameTextBox.Text = arr[1];
                        break;
                    case "VM_IP":
                        IpTextBox.Text = arr[1];
                        break;
                    default:
                        Console.WriteLine("This Setting is not supported");
                        break;
                }
            }
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);

            if (e.CloseReason == CloseReason.WindowsShutDown) return;

            // Confirm user wants to close
            switch (MessageBox.Show(this, "Are you sure you want to save these settings?", "Save", MessageBoxButtons.YesNo))
            {
                case DialogResult.No:
                    e.Cancel = true;
                    break;
                case DialogResult.Yes:
                    string[] settings = new string[5];

                    settings[0] = "Dynamic=" + ((bool)DynamicBtn.Checked ? "True" : "False");
                    settings[1] = "Static=" + ((bool)StaticBtn.Checked ? "True" : "False");
                    settings[2] = "Snapshot=" + SnapshotTextBox.Text.ToString();
                    settings[3] = "VM_Name=" + VMNameTextBox.Text.ToString();
                    settings[4] = "VM_IP=" + IpTextBox.Text.ToString();

                    System.IO.File.WriteAllLines(@"config.cfg", settings);
                    break;
                default:
                    break;
            }
        }
        /*
        private void Settings_FormClosing(object sender, CancelEventArgs e)
        {
            string[] settings = new string[4];

            settings[0] = "Dynamic=" + ((bool)DynamicBtn.Checked ? "True" : "False");
            settings[1] = "Static=" + ((bool)StaticBtn.Checked ? "True" : "False");
            settings[2] = "Snapshot=" + SnapshotTextBox.Text.ToString();
            settings[3] = "VM_Name=" + VMNameTextBox.Text.ToString();

            System.IO.File.WriteAllLines(@"config.cfg", settings);
        }
        */
    }
}
