﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System.IO;
using System.Threading;
using Newtonsoft.Json;
using Data = Google.Apis.Sheets.v4.Data;
using SandCastleGui.Properties;

namespace SandCastleGui
{
    class DoubleRunVerifier
    {
        static string[] Scopes = { SheetsService.Scope.Spreadsheets };
        static string ApplicationName = "Google Sheets API .NET Quickstart";
        static SheetsService service;
        static String spreadsheetId = "1a5Ur-R_uN1fz7Itb65mqljQv59cDZRpYRzeFgnjt7j0";

        public static void VerifyDoubleRun()
        {
            UserCredential credential;

            string appdataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            File.WriteAllText(appdataPath + @"\credentials.json", Resources.credentials);
            
            using (var stream =
                new FileStream(appdataPath + @"\credentials.json", FileMode.Open, FileAccess.Read))
            {
                // The file token.json stores the user's access and refresh tokens, and is created
                // automatically when the authorization flow completes for the first time.
                string credPath = appdataPath + @"\token.json";
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
                Console.WriteLine("Credential file saved to: " + credPath);
            }

            // Create Google Sheets API service.
            service = new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });            
        }

        public static string CheckHash(string hash)
        {
            IList<IList<Object>> values = GetLines();
            if (values != null && values.Count > 0)
            {
                foreach (var row in values)
                {
                    // Print columns A and E, which correspond to indices 0 and 4.
                    if((string)row[0] == hash)
                    {
                        return ((string)row[1] == "yes" ? "Safe" : "Malicious");
                    }
                }
                return "Not Found";
            }
            else
            {
                Console.WriteLine("No data found from sheets.");
                return "Not checked";
            }
        }

        public static void SendHash(string hash, string isOk)
        {
            string range = "Sheet1!A" + (GetLines().Count() + 1).ToString() + ":B";  // TODO: Update placeholder value.

            // How the input data should be interpreted.
            SpreadsheetsResource.ValuesResource.UpdateRequest.ValueInputOptionEnum valueInputOption = (SpreadsheetsResource.ValuesResource.UpdateRequest.ValueInputOptionEnum)1;  // TODO: Update placeholder value.

            // TODO: Assign values to desired properties of `requestBody`. All existing
            // properties will be replaced:
            Data.ValueRange requestBody = new Data.ValueRange();
            IList<IList<object>> value = new List<IList<object>>();
            value.Add(new List<object>());
            value[0].Add(hash);
            value[0].Add(isOk);
            //requestBody.Values.Add(value);
            requestBody.Values = value;
            SpreadsheetsResource.ValuesResource.UpdateRequest request = service.Spreadsheets.Values.Update(requestBody, spreadsheetId, range);
            request.ValueInputOption = valueInputOption;

            // To execute asynchronously in an async method, replace `request.Execute()` as shown:
            Data.UpdateValuesResponse response = request.Execute();
            // Data.UpdateValuesResponse response = await request.ExecuteAsync();

            // TODO: Change code below to process the `response` object:
            //Console.WriteLine(JsonConvert.SerializeObject(response));
        }

        public static IList<IList<Object>> GetLines()
        {
            String range = "Sheet1!A:B";
            SpreadsheetsResource.ValuesResource.GetRequest request =
                    service.Spreadsheets.Values.Get(spreadsheetId, range);

            // Prints the names and majors of students in a sample spreadsheet:
            // https://docs.google.com/spreadsheets/d/1a5Ur-R_uN1fz7Itb65mqljQv59cDZRpYRzeFgnjt7j0/edit
            ValueRange response = request.Execute();
            IList<IList<Object>> values = response.Values;

            return values;
        }
    }
}
