﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace SandCastleGui
{
    partial class Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DynamicBtn = new System.Windows.Forms.CheckBox();
            this.SettingsLabel = new System.Windows.Forms.Label();
            this.StaticBtn = new System.Windows.Forms.CheckBox();
            this.VMNameTextBox = new System.Windows.Forms.TextBox();
            this.SnapshotTextBox = new System.Windows.Forms.TextBox();
            this.VMNameLabel = new System.Windows.Forms.Label();
            this.SnapshotLabel = new System.Windows.Forms.Label();
            this.IpTextBox = new System.Windows.Forms.TextBox();
            this.IpLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // DynamicBtn
            // 
            this.DynamicBtn.AutoSize = true;
            this.DynamicBtn.ForeColor = System.Drawing.Color.White;
            this.DynamicBtn.Location = new System.Drawing.Point(327, 168);
            this.DynamicBtn.Name = "DynamicBtn";
            this.DynamicBtn.Size = new System.Drawing.Size(108, 17);
            this.DynamicBtn.TabIndex = 0;
            this.DynamicBtn.Text = "Dynamic Analysis";
            this.DynamicBtn.UseVisualStyleBackColor = true;
            // 
            // SettingsLabel
            // 
            this.SettingsLabel.AutoSize = true;
            this.SettingsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 45F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SettingsLabel.ForeColor = System.Drawing.Color.White;
            this.SettingsLabel.Location = new System.Drawing.Point(269, 57);
            this.SettingsLabel.Name = "SettingsLabel";
            this.SettingsLabel.Size = new System.Drawing.Size(247, 69);
            this.SettingsLabel.TabIndex = 1;
            this.SettingsLabel.Text = "Settings";
            // 
            // StaticBtn
            // 
            this.StaticBtn.AutoSize = true;
            this.StaticBtn.ForeColor = System.Drawing.Color.White;
            this.StaticBtn.Location = new System.Drawing.Point(327, 191);
            this.StaticBtn.Name = "StaticBtn";
            this.StaticBtn.Size = new System.Drawing.Size(94, 17);
            this.StaticBtn.TabIndex = 2;
            this.StaticBtn.Text = "Static Analysis";
            this.StaticBtn.UseVisualStyleBackColor = true;
            // 
            // VMNameTextBox
            // 
            this.VMNameTextBox.Location = new System.Drawing.Point(350, 214);
            this.VMNameTextBox.Name = "VMNameTextBox";
            this.VMNameTextBox.Size = new System.Drawing.Size(100, 20);
            this.VMNameTextBox.TabIndex = 3;
            // 
            // SnapshotTextBox
            // 
            this.SnapshotTextBox.Location = new System.Drawing.Point(350, 240);
            this.SnapshotTextBox.Name = "SnapshotTextBox";
            this.SnapshotTextBox.Size = new System.Drawing.Size(100, 20);
            this.SnapshotTextBox.TabIndex = 4;
            // 
            // VMNameLabel
            // 
            this.VMNameLabel.AutoSize = true;
            this.VMNameLabel.BackColor = System.Drawing.Color.Transparent;
            this.VMNameLabel.ForeColor = System.Drawing.Color.White;
            this.VMNameLabel.Location = new System.Drawing.Point(290, 217);
            this.VMNameLabel.Name = "VMNameLabel";
            this.VMNameLabel.Size = new System.Drawing.Size(54, 13);
            this.VMNameLabel.TabIndex = 5;
            this.VMNameLabel.Text = "VM Name";
            // 
            // SnapshotLabel
            // 
            this.SnapshotLabel.AutoSize = true;
            this.SnapshotLabel.BackColor = System.Drawing.Color.Transparent;
            this.SnapshotLabel.ForeColor = System.Drawing.Color.White;
            this.SnapshotLabel.Location = new System.Drawing.Point(290, 243);
            this.SnapshotLabel.Name = "SnapshotLabel";
            this.SnapshotLabel.Size = new System.Drawing.Size(52, 13);
            this.SnapshotLabel.TabIndex = 6;
            this.SnapshotLabel.Text = "Snapshot";
            // 
            // IpTextBox
            // 
            this.IpTextBox.Location = new System.Drawing.Point(350, 266);
            this.IpTextBox.Name = "IpTextBox";
            this.IpTextBox.Size = new System.Drawing.Size(100, 20);
            this.IpTextBox.TabIndex = 7;
            // 
            // IpLabel
            // 
            this.IpLabel.AutoSize = true;
            this.IpLabel.BackColor = System.Drawing.Color.Transparent;
            this.IpLabel.ForeColor = System.Drawing.Color.White;
            this.IpLabel.Location = new System.Drawing.Point(306, 269);
            this.IpLabel.Name = "IpLabel";
            this.IpLabel.Size = new System.Drawing.Size(36, 13);
            this.IpLabel.TabIndex = 8;
            this.IpLabel.Text = "VM IP";
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.IpLabel);
            this.Controls.Add(this.IpTextBox);
            this.Controls.Add(this.SnapshotLabel);
            this.Controls.Add(this.VMNameLabel);
            this.Controls.Add(this.SnapshotTextBox);
            this.Controls.Add(this.VMNameTextBox);
            this.Controls.Add(this.StaticBtn);
            this.Controls.Add(this.SettingsLabel);
            this.Controls.Add(this.DynamicBtn);
            this.Name = "Settings";
            this.Text = "Settings";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox DynamicBtn;
        private System.Windows.Forms.Label SettingsLabel;
        private System.Windows.Forms.CheckBox StaticBtn;
        private System.Windows.Forms.TextBox VMNameTextBox;
        private System.Windows.Forms.TextBox SnapshotTextBox;
        private System.Windows.Forms.Label VMNameLabel;
        private System.Windows.Forms.Label SnapshotLabel;
        private TextBox IpTextBox;
        private Label IpLabel;
    }
}