﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Pipes;
using System.Diagnostics;
using System.Threading;

namespace SandCastleGui
{
    class Encryption
    {
        public static void ConnectPipe(SandCastle myWin)
        {
            NamedPipeServerStream pipe = new NamedPipeServerStream("SandCastlePipe", PipeDirection.InOut);

            //launch .py

            pipe.WaitForConnectionAsync();

            Process proc = new Process();
            proc.StartInfo.FileName = @"key_exchanger.exe";
            proc.StartInfo.RedirectStandardOutput = true;
            proc.StartInfo.UseShellExecute = false;
            proc.Start();

            Thread t = new Thread(new ParameterizedThreadStart(Program.printProcessFunc));
            t.Start(proc);

            //run_cmd("key_exchanger.py");
            System.Threading.Thread.Sleep(1500);
            try
            {
                StreamReader sr = new StreamReader(pipe);
                StreamWriter sw = new StreamWriter(pipe);
                string to_send;
                byte[] key1 = new byte[2048];

                //to_send = getPartkey(sr);
                //myWin.s.Send(Encoding.ASCII.GetBytes(to_send));
                //Console.WriteLine($"p = {to_send}");
                to_send = getPartkey(sr);
                myWin.s.Send(Encoding.ASCII.GetBytes(to_send));
                Console.WriteLine($"g = {to_send}");

                to_send = getPartkey(sr);
                myWin.s.Send(Encoding.ASCII.GetBytes(to_send));
                Console.WriteLine($"a1 = {to_send}");

                myWin.s.Receive(key1);

                to_send = "";
                for (int i = 0; i < 2048; i++)
                {
                    if (key1[i] != 0)
                    {
                        to_send += (char)key1[i];
                    }
                }
                sw.WriteLine(to_send);
                sw.Flush();
                to_send = getPartkey(sr);


                Console.WriteLine("Named pipe returned final key as: " + to_send);
                
                myWin.str_key = sha256(to_send);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Data.ToString());
            }
        }

        public static string getPartkey(StreamReader sr)
        {
            string to_send = "";
            char[] key1 = new char[2048];
            sr.Read(key1, 0, 2048);
            for (int i = 0; i < 2048; i++)
            {
                if (key1[i] != 0)
                {
                    to_send += Convert.ToString(key1[i]);
                }
            }
            return to_send;
            
        }

        private static void run_cmd(string cmd)
        {
            ProcessStartInfo start = new ProcessStartInfo();
            start.FileName = @"C:\Python36\python3.exe";
            start.Arguments = string.Format("{0}", cmd);
            start.UseShellExecute = false;
            start.RedirectStandardError = true;
            Process process = Process.Start(start);
        }

        public static void Check()
        {
            try
            {
                string original = "Here is some data to encrypt!";

                // Create a new instance of the Aes 
                // class.  This generates a new key and initialization  
                // vector (IV). 
                using (var random = new RNGCryptoServiceProvider())
                {
                    var key = new byte[16];
                    var str_key = "LKHlhb899Y09olUi";
                    key = Encoding.ASCII.GetBytes(str_key);


                    // Encrypt the string to an array of bytes. 
                    byte[] encrypted = EncryptStringToBytes_Aes(original, key);

                    // Decrypt the bytes to a string. 
                    string roundtrip = DecryptStringFromBytes_Aes(encrypted, key);

                    //Display the original data and the decrypted data.
                    Console.WriteLine("Original:   {0}", original);
                    Console.WriteLine("Encrypted (b64-encode): {0}", Convert.ToBase64String(encrypted));
                    Console.WriteLine("Round Trip: {0}", roundtrip);
                }

            }
            catch (Exception e)
            {
                Console.WriteLine("Error: {0}", e.Message);
            }
        }

        static string sha256(string randomString)
        {
            var crypt = new SHA256Managed();
            string hash = String.Empty;
            byte[] crypto = crypt.ComputeHash(Encoding.ASCII.GetBytes(randomString));
            foreach (byte theByte in crypto)
            {
                hash += theByte.ToString("x2");
            }
            return hash.Substring(0 ,16);
        }

        public static byte[] EncryptStringToBytes_Aes(string plainText, byte[] Key)
        {
            byte[] encrypted;
            byte[] IV;

            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = Key;

                aesAlg.GenerateIV();
                IV = aesAlg.IV;

                aesAlg.Mode = CipherMode.CBC;

                var encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for encryption. 
                using (var msEncrypt = new MemoryStream())
                {
                    using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (var swEncrypt = new StreamWriter(csEncrypt))
                        {
                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }

            var combinedIvCt = new byte[IV.Length + encrypted.Length];
            Array.Copy(IV, 0, combinedIvCt, 0, IV.Length);
            Array.Copy(encrypted, 0, combinedIvCt, IV.Length, encrypted.Length);

            // Return the encrypted bytes from the memory stream. 
            string b64 =  Convert.ToBase64String(combinedIvCt);
            return Encoding.ASCII.GetBytes(b64);

        }

        public static string DecryptStringFromBytes_Aes(byte[] cipherTextCombined, byte[] Key)
        {

            // Declare the string used to hold 
            // the decrypted text. 
            
            string plaintext = null;

            // Create an Aes object 
            // with the specified key and IV. 
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = Key;

                byte[] IV = new byte[aesAlg.BlockSize / 8];
                byte[] cipherText = new byte[cipherTextCombined.Length - IV.Length];

                Array.Copy(cipherTextCombined, IV, IV.Length);
                Array.Copy(cipherTextCombined, IV.Length, cipherText, 0, cipherText.Length);

                aesAlg.IV = IV;

                aesAlg.Mode = CipherMode.CBC;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for decryption. 
                using (var msDecrypt = new MemoryStream(cipherText))
                {
                    using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (var srDecrypt = new StreamReader(csDecrypt))
                        {

                            // Read the decrypted bytes from the decrypting stream
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }

            }

            return plaintext;

        }
    }
}
