﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Threading;
using System.Diagnostics;

namespace SandCastleGui
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>

        //define necessary funcs for console hiding 
        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        const int SW_HIDE = 0;
        const int SW_SHOW = 5;

        [DllImport("user32.dll")]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);





        [STAThread]
        static void Main()
        {
            string[] args = Environment.GetCommandLineArgs();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            try
            {
                int largestWidth = Console.LargestWindowWidth;
                int largestHeight = Console.LargestWindowHeight;
                Console.SetWindowSize(largestWidth/2, largestHeight);
                string text = System.IO.File.ReadAllText(@"AsciiArt.txt");
                Console.Out.WriteAsync(text);
                Thread.Sleep(2500);

            }
            catch(Exception e)
            {
            }
            if (args.Length == 1)
            {
                var hWnd = FindWindow(null, System.Windows.Forms.Application.ExecutablePath);
                if(hWnd != IntPtr.Zero)
                {
                    ShowWindow(hWnd, 0); // 0 = SW_HIDE
                }
                Application.Run(new SandCastle());
                //var handle = GetConsoleWindow();//create console handler
                //ShowWindow(handle, SW_HIDE);//hide the window via the show func with hide flag

            }
            else
            {
                Console.WriteLine("\nGot " + args.Length + " arguments, executing commands in CLI.\n\n");
                System.Threading.Thread.Sleep(2000);

                CommandLine.commandLineInterface(args);
                Console.ReadLine();
            }
        }

        public static void printProcessFunc(object p)
        {
            var proc = (Process)p;
            while (!proc.StandardOutput.EndOfStream)
            {
                Console.WriteLine(proc.StandardOutput.ReadLine());
            }
        }
    }

    static class CommandLine
    {
        public static void commandLineInterface(string[] args)
        {
            printUsage();
            //TODO: add checking for settings
            checkSettings(args);
        }

        private static void checkSettings(string[] args)
        {
            string[] settings = new string[4];
            bool dynamicTest = false, staticTest = false, toBreak = false;
            SandCastle sand = new SandCastle();

            foreach(string str in args)
            {
                if(str == "-f")
                {
                    sand.filePath = args[Array.IndexOf(args, str) + 1];
                }
                else if(str == "-s")
                {
                    switch (args[Array.IndexOf(args, str) + 1])
                    {
                        case "S":
                            dynamicTest = false;
                            staticTest = true;
                            break;
                        case "D":
                            dynamicTest = true;
                            staticTest = false;
                            break;
                        case "SD":
                            dynamicTest = true;
                            staticTest = true;
                            break;
                        default:
                            toBreak = true;
                            break;
                    }
                    settings[0] = "Dynamic=" + (dynamicTest ? "True" : "False");
                    settings[1] = "Static=" + (staticTest ? "True" : "False");
                }
                else if(str == "-vm")
                {
                    settings[3] = "VM_Name=" + args[Array.IndexOf(args, str) + 1];
                }
                else if(str == "-snap")
                {
                    settings[2] = "Snapshot=" + args[Array.IndexOf(args, str) + 1];
                }

                if(toBreak)
                {
                    printUsage();
                    break;
                }
            }

            if(!toBreak)
            {
                System.IO.File.WriteAllLines(@"config.cfg", settings);
                Console.WriteLine("Youe command will now execute! Any error under CLI is your own responsebility.\n");
                sand.UploadToServer_Click(null, null);
            }
        }

        private static void printUsage()
        {
            Console.WriteLine("Welcome to the SandCastle adaptive sandbox!\n");
            Console.WriteLine("If you choose to start the program with args and not use the user interface,\n" +
                "you should be aware of it's usuage...\n\n\n" +
                "-f\tA flag that represents the file that will be sent to the sandbox for checking.\n" + 
                "  \tThe property of this flag should be in quotation marks.\n\n" +
                "-s\tA flag that controls the settings of the sandbox. S for static analysis only, D for dynamic analysis only,\n" +
                "  \tSD for static and dynamic analysis. Default is SD.\n\n" +
                "-vm\tA flag that controls the name of the VM that should be executed, e.g. -vm \"Windows 10 dev environment\".\n" +
                "   \tThe property of this flag should be in quotation marks.\n\n" +
                "-snap\tA flag that controls which snapshot should be run on the given VM, e.g. -snap \"snapshot1\".\n" +
                "     \tThe property of this flag should be in quotation marks.\n\n\n\n" +
                "If any of these properties will not be disclosed when given args, the settings will be taken from the config file.\n" +
                "Not filling in the settigns in CLI may lead to errors executing the VM.");
        }

    }

}
