using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace SandCastleGui
{
    public partial class SandCastle : Form
    {

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;
        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();


        public Socket s;
        public string filePath { set; get; }

        private Dictionary<string, string> extentions = new Dictionary<string, string>() { { "exe", "1" } };//, { "dll", "2" }, { "bat", "3" }, { "vbs", "4" }, { "com", "5" }, { "pyc", "6" }, { "py", "7" }, { "else", "8" } };
        private Dictionary<string, string> commands = new Dictionary<string, string>() { { "send_file", "1" }, { "stop_run", "2" }, { "send_log", "3" }, { "disconnect", "4" }, { "le_supriso", "1337" } };
        private Dictionary<string, string> parameters = new Dictionary<string, string>() { { "proceed_to_dynamic", "1" }, { "force_dynamic", "2" }, { "skip_static", "3" }, { "limit_to_static", "4" } };

        public string str_key = "LKHlhb899Y09olUi";
        byte[] key = new byte[16];
        private OpenFileDialog openFileDialog1;

        private string IP = "";

        public SandCastle()
        {
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;

            InitializeComponent();
            IP = getValueFromConfig("VM_IP");
            this.AllowDrop = true;
            this.DragEnter += new DragEventHandler(Form1_DragEnter);
            this.DragDrop += new DragEventHandler(Form1_DragDrop);

            DoubleRunVerifier.VerifyDoubleRun();

        }

        public void Form1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Copy;

            }
        }

        public void Form1_DragDrop(object sender, DragEventArgs e)
        {
            string[] file = (string[])e.Data.GetData(DataFormats.FileDrop);
            //this.FileNameLabel.Text = file[0];
            cutFileName(file[0]);
        }

        private void UploadFile_Click(object sender, EventArgs e)
        {
            openFileDialog1 = new OpenFileDialog();
            DialogResult dlg = openFileDialog1.ShowDialog();

            // Process open file dialog box results
            if (dlg == DialogResult.OK)
            {
                // Open document
                string filename = openFileDialog1.FileName;
                cutFileName(filename);
            }
        }

        private void cutFileName(string filename)
        {
            filePath = filename;
            filename = Path.GetFileName(filename);
            this.FileNameLabel.Text = filename;
        }


        private bool openProcessWithSocket()
        {
            s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            IPEndPoint remoteEP = new IPEndPoint(IPAddress.Parse(IP), 1337);

            try
            {
                s.Connect(remoteEP);
            }
            catch (Exception e)
            {
                MessageBox.Show("Could not connect to the network!");
                Console.WriteLine("Unexpected exception {0}", e.ToString());
                return false;
            }
            return true;
        }

        private byte[] readFileToByteArray(string filename)
        {
            byte[] fileBytes = File.ReadAllBytes(filename);
            return fileBytes;
        }

        private string byteArrayToBase64(byte[] arr)
        {
            return Convert.ToBase64String(arr);
        }


        public void UploadToServer_Click(object sender, EventArgs e)
        {
            var st = DoubleRunVerifier.CheckHash(GetChecksum(filePath));
            if (st == "Malicious" || st == "Safe")
            {
                switch(st)
                {
                    case "Malicious":
                        MessageBox.Show("File was already checked. He is malicious!", "SandCastle", MessageBoxButtons.OK);
                        break;

                    case "Safe":
                        MessageBox.Show("File was already checked. He is safe to use!", "SandCastle", MessageBoxButtons.OK);
                        break;

                    default:
                        MessageBox.Show("OK WTF? that shouldn't happen", "SandCastle", MessageBoxButtons.OK);
                        break;
                }
                return;
            }
            if(!openProcessWithSocket())
                return;
            Encryption.ConnectPipe(this);
            key = Encoding.ASCII.GetBytes(str_key);

            string b64data = byteArrayToBase64(readFileToByteArray(filePath));
            Dictionary<string, string> msg = new Dictionary<string, string>();

            msg["data"] = b64data;
            //msg["extention"] = extentions[Path.GetExtension(filePath).Substring(1)];
            msg["extention"] = extentions["exe"];
            string DynamicAnal = getValueFromConfig("Dynamic");
            string StaticAnal = getValueFromConfig("Static");
            if(DynamicAnal == "True" && StaticAnal == "True")
                msg["run_type"] = "0";
            else if(DynamicAnal == "True")
                msg["run_type"] = "2";
            else if (StaticAnal == "True")
                msg["run_type"] = "1";
            msg["command"] = commands["send_file"];

            sendData(getJsonFormat(msg));
            string finish = "Finished";

            s.Send(Encoding.ASCII.GetBytes(finish));

            FileNameLabel.Text = "Running on Server";
            
            ((Button)sender).Enabled = false;

            //recv the output log
            Thread thread = new Thread(new ParameterizedThreadStart(getLog));
            thread.Start(sender);

            //check with virus total
            Thread virusTotal = new Thread(new ThreadStart(virusTotalCheck));
            virusTotal.Start();

            Thread t = new Thread(new ThreadStart(runStream));
            t.Start();
        }

        private void virusTotalCheck()
        {
            Process p = new Process();
            p.StartInfo.FileName = "virustotal.exe";
            p.StartInfo.Arguments = filePath;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.UseShellExecute = false;
            p.Start();
            Program.printProcessFunc(p);
        }

        private void runStream()
        {
            Process proc = new Process();
            proc.StartInfo.FileName = "stream.exe";
            proc.StartInfo.Arguments = IP;
            proc.StartInfo.RedirectStandardOutput = true;
            proc.StartInfo.UseShellExecute = false;
            proc.Start();
            Program.printProcessFunc(proc);
        }


        private void getLog(object sendButton)
        {
            try
            {


                FileStream logFile = new FileStream("log.txt", FileMode.Append);
                string data = "";

                while (!data.Contains("Finished"))
                {
                    byte[] bytes = new byte[1024];
                    int bytesRec = s.Receive(bytes);
                    string b64 = Encoding.ASCII.GetString(bytes);
                    data += b64;
                }


                data = data.Substring(0, data.LastIndexOf("Finished"));
                string converted = data.Replace('-', '+');
                converted = converted.Replace('_', '/');
                converted = converted.Replace("\0", string.Empty);
                converted = converted.Replace(@"\n", "");
                converted = Pad(converted);
                byte[] dataBytes = Convert.FromBase64String(converted);
                data = Encryption.DecryptStringFromBytes_Aes(dataBytes, key);

                Console.WriteLine("the log is:" + data);
                logFile.Write(Encoding.ASCII.GetBytes(data), 0, data.Length);
                logFile.Close();

                logAnalysisAndFeedback(new Tuple<string, object>(data, sendButton));
            }
            catch(Exception e)
            {
                MessageBox.Show("Server Connection Error");
                Application.Exit();
                Console.WriteLine($"Exception accured {e}");
            }
        }

        private void logAnalysisAndFeedback(Tuple<string, object> tuple)
        {
            string data = tuple.Item1;
            object sendButton = tuple.Item2;
            Process proc = new Process();
            proc.StartInfo.FileName = @"logAnalysis.exe";
            proc.StartInfo.RedirectStandardOutput = true;
            proc.StartInfo.UseShellExecute = false;
            proc.Start();

            Thread t = new Thread(new ParameterizedThreadStart(Program.printProcessFunc));
            t.Start(proc);

            t.Join();

            Thread.Sleep(1500);

            string json = File.ReadAllText("analysisOutput.txt");
            string virusTotalOutput = File.ReadAllText("virusTotalOutput.txt");

            Dictionary<string, string> output = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            string virusTotal = "Virus total finished with negative result (is not a malware by virus total)";
            if (virusTotalOutput != "0")
            {
                output["level"] = "2";
                virusTotal = "Virus total finished with positive result (it is a malware)";
            }


            switch (output["level"])
            {
                case "0":
                    DoubleRunVerifier.SendHash(GetChecksum(filePath), "yes");
                    break;

                case "1":
                    DoubleRunVerifier.SendHash(GetChecksum(filePath), "suspicious");
                    break;

                case "2":
                    DoubleRunVerifier.SendHash(GetChecksum(filePath), "no");
                    break;

                default:
                    Console.WriteLine("LOL that's wrong Alexander");
                    break;
            }


            string filename = Path.GetFileName(filePath);


            proc.StartInfo.FileName = @"report.exe";
            proc.StartInfo.Arguments = filename + " " + GetChecksum(filePath) + " \"" + output["malicious_activity"] + "\" \"" + virusTotal + "\" log.txt";
            proc.StartInfo.RedirectStandardOutput = true;
            proc.StartInfo.UseShellExecute = false;
            proc.Start();

            t = new Thread(new ParameterizedThreadStart(Program.printProcessFunc));
            t.Start(proc);
            Thread.Sleep(4000);

            Process.Start("SandCastleOutput.html");
            s.Close();

            this.Invoke((MethodInvoker)delegate { ((Button)sendButton).Enabled = true; });
        }

        private string Pad(string converted)
        {
            return (converted.Length % 4 != 0) ? converted + new string('=', 4 - converted.Length % 4) : converted;
        }

        private string getJsonFormat(Dictionary<string, string> dict)
        {
            return JsonConvert.SerializeObject(dict);
        }

        private static string GetChecksum(string file)
        {
            using (FileStream stream = File.OpenRead(file))
            {
                var sha = new SHA256Managed();
                byte[] checksum = sha.ComputeHash(stream);
                return BitConverter.ToString(checksum).Replace("-", String.Empty);
            }
        }


        private void sendData(string data)
        {
            try
            {

            //byte[] msg = Encoding.ASCII.GetBytes(data);
            byte[] msg = Encryption.EncryptStringToBytes_Aes(data, key);
            int bytesSent = s.Send(msg);
            if (bytesSent > 0)
            {
                Console.WriteLine("Message Sent");
            }
            else
            {
                Console.WriteLine("Console isnt sent or send 0 bytes");
            }
            }
            catch(Exception e)
            {
                MessageBox.Show("Could not send data to the server", "Server Connection Error");
                Application.Exit();

                Console.WriteLine($"Exception accured {e}");
            }
        }

        private string recvData()
        {
            byte[] bytes = new byte[1024];
            int bytesRec = s.Receive(bytes);
            string b64 = Encoding.ASCII.GetString(bytes);
            bytes = Convert.FromBase64String(b64);
            string data = Encryption.DecryptStringFromBytes_Aes(bytes, key);
            Console.WriteLine("Data recieved");

            return data;
        }

        private void Power_On(object sender, EventArgs e)
        {
            string param = "-command \" & { &\"Restore-VMCheckpoint -Name \\\"snapshot1\\\" -VMName \\\"Windows 10 dev environment\\\" -Confirm:$false ; Start-VM -Name \\\"Windows 10 dev environment\\\"\"}";
            Process proc = new Process();
            proc.StartInfo.FileName = "Powershell.exe";
            proc.StartInfo.Arguments = param;
            proc.StartInfo.UseShellExecute = true;
            proc.StartInfo.Verb = "runas";
            proc.Start();
        }

        private string getValueFromConfig(string value)
        {
            string[] settings = System.IO.File.ReadAllLines(@"config.cfg");
            foreach (string line in settings)
            {
                string[] arr = line.Split('=');
                if (arr[0] == value)
                {
                    return arr[1];
                }
            }
            return "";
        }

        private void Window_Drop(object sender, DragEventArgs e)
        {
            string[] droppedFiles = null;
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                droppedFiles = e.Data.GetData(DataFormats.FileDrop, true) as string[];
                filePath = droppedFiles[0].ToString();
                FileNameLabel.Text = Path.GetFileName(filePath).ToString();
            }

        }


        private void button1_Click(object sender, EventArgs e)
        {
            Settings settings = new Settings();
            this.Hide();
            settings.Show();
            this.Show();
        }

        private void SandCastle_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
        }

        private void Button1_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }

}
