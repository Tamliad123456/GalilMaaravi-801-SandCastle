﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
//using System.Windows.Shapes;
using System.IO;
using System.Diagnostics;
using System.IO.Pipes;
using System.Net.Sockets;
using System.Net;
using Newtonsoft.Json;
using System.Security.Cryptography;

namespace SandCastleGui.Net
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Socket s;
        private string filePath;

        private Dictionary<string, string> extentions = new Dictionary<string, string>() { { "exe", "1" }, { "dll", "2" }, { "bat", "3" }, { "vbs", "4" }, { "com", "5" }, { "pyc", "6" }, { "py", "7" }, { "else", "8" } };
        private Dictionary<string, string> commands = new Dictionary<string, string>() { { "send_file", "1" }, { "stop_run", "2" }, { "send_log", "3" }, { "disconnect", "4" }, { "le_supriso", "1337" } };
        private Dictionary<string, string> parameters = new Dictionary<string, string>() { {"proceed_to_dynamic", "1" }, { "force_dynamic", "2" }, { "skip_static", "3" }, { "limit_to_static", "4" } };

        public string str_key = "LKHlhb899Y09olUi";
        byte[] key = new byte[16];

        public MainWindow()
        {
            InitializeComponent();
            openProcessWithSocket();
            Encryption.ConnectPipe();
            key = Encoding.ASCII.GetBytes(str_key);
        }
        

        private void UploadFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.DefaultExt = ".exe"; // Default file extension
            dlg.Filter = "PE (.exe)|*"; // Filter files by extension

            // Show open file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process open file dialog box results
            if (result == true)
            {
                // Open document
                string filename = dlg.FileName;
                filePath = filename;
                filename = Path.GetFileName(filename);
                this.FileNameLabel.Content = filename;
            }
        }


        private void openProcessWithSocket()
        {
            s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            IPEndPoint remoteEP = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 1337);

            try
            {
                s.Connect(remoteEP);
            }
            catch(Exception e)
            {
                Console.WriteLine("Unexpected exception {0}", e.ToString());
            }

        }

        private int exchangeKey()
        {
            Random rnd = new Random();
            UInt32 p = Convert.ToUInt32(rnd.Next(1, 4500));
            UInt32 g = Convert.ToUInt32(rnd.Next(1, 1500));
            sendData(p.ToString());
            sendData(g.ToString());
            UInt32 a = Convert.ToUInt32(rnd.Next(1, 4500));
            UInt32 a1 = Convert.ToUInt32(Math.Pow(g,a) % p);
            sendData(a1.ToString());
            long a2 = Convert.ToUInt32(recvData());

            return Convert.ToInt32(Math.Pow(a2,a) % p);
        }

        private byte[] readFileToByteArray(string filename)
        {
            byte[] fileBytes = File.ReadAllBytes(filename);
            return fileBytes;
        }

        private string byteArrayToBase64(byte[] arr)
        {
            return Convert.ToBase64String(arr);
        }


        private void UploadToServer_Click(object sender, RoutedEventArgs e)
        {
            if((string)Upload.Content == "Upload To Server")
            { 
                string b64data = byteArrayToBase64(readFileToByteArray(filePath));
                Dictionary<string, string> msg = new Dictionary<string, string>();

                msg["data"] = b64data;
                msg["extention"] = extentions[Path.GetExtension(filePath).Substring(1)];

                if (msg["extention"] == extentions["else"])
                {
                    msg["params"] = parameters["limit_to_static"];
                }
                msg["command"] = commands["send_file"];

                sendData(getJsonFormat(msg));
                string finish = "Finished";
                
                s.Send(Encoding.ASCII.GetBytes(finish));

                FileNameLabel.Content = "Running on Server";
                Upload.Content = "Cancel Run";
            }
            else
            {
                Dictionary<string, string> msg = new Dictionary<string, string>();

                msg["command"] = commands["stop_run"];

                sendData(getJsonFormat(msg));

                getLog();

                FileNameLabel.Content = "Log file exported to current folder";
                Upload.Content = "Upload To Server";
            }
        }
        

        private void getLog()
        {
            FileStream logFile = new FileStream("log.txt", FileMode.Append);
            string data = "";

            while (data != "Finished")
            {
                logFile.WriteAsync(Encoding.ASCII.GetBytes(data), 0, data.Length);
            }

            DoubleRunVerifier.SendHash(GetChecksum(filePath), data[0].ToString());
        }

        private string getJsonFormat(Dictionary<string, string> dict)
        {
            return JsonConvert.SerializeObject(dict);
        }

        private static string GetChecksum(string file)
        {
            using (FileStream stream = File.OpenRead(file))
            {
                var sha = new SHA256Managed();
                byte[] checksum = sha.ComputeHash(stream);
                return BitConverter.ToString(checksum).Replace("-", String.Empty);
            }
        }


        private void sendData(string data)
        {
            //byte[] msg = Encoding.ASCII.GetBytes(data);
            byte[] msg = Encryption.EncryptStringToBytes_Aes(data, key);
            int bytesSent = s.Send(msg);
            if(bytesSent > 0)
            {
                Console.WriteLine("Message Sent");
            }
            else
            {
                Console.WriteLine("Console isnt sent or send 0 bytes");
            }
        }

        private string recvData()
        {
            byte[] bytes = new byte[1024];
            int bytesRec = s.Receive(bytes);
            string b64 = Encoding.ASCII.GetString(bytes);
            bytes = Convert.FromBase64String(b64);
            string data = Encryption.DecryptStringFromBytes_Aes(bytes, key);
            //string data = Encoding.ASCII.GetString(bytes, 0, bytesRec);
            Console.WriteLine("The recvied data = {0}", data);

            return data;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Hide();
            Settings win = new Settings();
            win.ShowDialog();
            Show();
        }

        private void Power_On(object sender, RoutedEventArgs e)
        {
            string param = "-command \" & { &\"Restore-VMCheckpoint -Name \\\"snapshot1\\\" -VMName \\\"Windows 10 dev environment\\\" -Confirm:$false ; Start-VM -Name \\\"Windows 10 dev environment\\\"\"}";
            Process proc = new Process();
            proc.StartInfo.FileName = "Powershell.exe";
            proc.StartInfo.Arguments = param;
            proc.StartInfo.UseShellExecute = true;
            proc.StartInfo.Verb = "runas";
            proc.Start();
        }

        private string getValueFromConfig(string value)
        {
            string[] settings = System.IO.File.ReadAllLines(@"config.cfg");
            foreach(string line in settings)
            {
                string[] arr = line.Split('=');
                if(arr[0] == value)
                {
                    return arr[1];
                }
            }
            return "";
        }

        private void Window_Drop(object sender, DragEventArgs e)
        {
            string[] droppedFiles = null;
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                droppedFiles = e.Data.GetData(DataFormats.FileDrop, true) as string[];
                filePath = droppedFiles[0].ToString();
                FileNameLabel.Content = Path.GetFileName(filePath).ToString();
            }

        }


        string Encrypt(string textToEncrypt, string passphrase)
        {
            RijndaelManaged rijndaelCipher = new RijndaelManaged();
            rijndaelCipher.Mode = CipherMode.CBC;
            rijndaelCipher.Padding = PaddingMode.PKCS7;

            rijndaelCipher.KeySize = 128;
            rijndaelCipher.BlockSize = 128;
            byte[] pwdBytes = Encoding.UTF8.GetBytes(passphrase);
            byte[] keyBytes = new byte[16];
            int len = pwdBytes.Length;
            if (len > keyBytes.Length)
            {
                len = keyBytes.Length;
            }
            Array.Copy(pwdBytes, keyBytes, len);
            rijndaelCipher.Key = keyBytes;
            rijndaelCipher.IV = new byte[16];
            ICryptoTransform transform = rijndaelCipher.CreateEncryptor();
            byte[] plainText = Encoding.UTF8.GetBytes(textToEncrypt);
            return Convert.ToBase64String(transform.TransformFinalBlock(plainText, 0, plainText.Length));
        }
    }
}
