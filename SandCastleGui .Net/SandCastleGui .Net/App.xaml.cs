﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace SandCastleGui.Net
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        string[] args;
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            MainWindow wnd = new MainWindow();
            if (e.Args.Length > 0)
            {
                for(int i = 0; i< e.Args.Length; i++)
                {
                    args[i] = e.Args[i];
                }
            }
            else
            {
                wnd.Show();
            }
        }
    }
}
