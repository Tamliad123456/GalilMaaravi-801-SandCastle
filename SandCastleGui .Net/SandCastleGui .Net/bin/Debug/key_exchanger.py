import random

def exchange_keys():
    with open("\\\\.\\pipe\\SandCastlePipe", 'w+b', buffering=0) as pipe:
       p = 43579
       g = random.randint(1, 4500)
       a1 = random.randint(1, 4500)
       #pipe.write(bytes(str(p), "ASCII"))
       pipe.write(bytes(str(g), "ASCII"))

       print(f"p = {p}, g = {g}, a1 = {a1}")
       a = (g**a1) % p
       pipe.write(bytes(str(a), "ASCII"))
       print(f"write {str(a)}")
       a2 = int(pipe.read(1024))
       print(f"a2 = {a2}")
       pipe.write(bytes(str((a2**a1) % p), "ASCII"))
       print(f"write {str((a2**a1) % p)}")


exchange_keys()