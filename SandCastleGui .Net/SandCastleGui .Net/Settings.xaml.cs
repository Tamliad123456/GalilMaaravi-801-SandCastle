﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;

namespace SandCastleGui.Net
{
    /// <summary>
    /// Interaction logic for Settings.xaml
    /// </summary>
    public partial class Settings : Window
    {
        public string VMName = "";
        public string SnapshotName = "";
        public Settings()
        {
            Closing += Settings_FormClosing;
            InitializeComponent();
            string[] settings = System.IO.File.ReadAllLines(@"config.cfg");

            foreach(string setting in settings)
            {
                string[] arr = setting.Split('=');
                switch (arr[0])
                {
                    case "Dynamic":
                        if(arr[1] == "True")
                        {
                            DynamicCheck.IsChecked = true;
                        }
                        else if(arr[1] == "False")
                        {
                            DynamicCheck.IsChecked = false;
                        }
                        break;
                    case "Static":
                        if (arr[1] == "True")
                        {
                            StaticCheck.IsChecked = true;
                        }
                        else if (arr[1] == "False")
                        {
                            StaticCheck.IsChecked = false;
                        }
                        break;
                    case "Snapshot":
                        SnapshotText.Text = arr[1];
                        break;
                    case "VM_Name":
                        VMNameText.Text = arr[1];
                        break;
                    default:
                        Console.WriteLine("This Setting is not supported");
                        break;
                }
            }
        }

        private void Settings_FormClosing(object sender, CancelEventArgs e)
        {
            string[] settings = new string[4];

            settings[0] = "Dynamic=" + ((bool)DynamicCheck.IsChecked ? "True" : "False");
            settings[1] = "Static=" + ((bool)StaticCheck.IsChecked ? "True" : "False");
            settings[2] = "Snapshot=" + SnapshotText.Text.ToString();
            settings[3] = "VM_Name=" + VMNameText.Text.ToString();

            System.IO.File.WriteAllLines(@"config.cfg", settings);
        }
    }
}
