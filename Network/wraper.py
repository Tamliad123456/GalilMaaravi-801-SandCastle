import os
import gspread
from oauth2client.service_account import ServiceAccountCredentials

scope = ['https://spreadsheets.google.com/feeds']
creds = ServiceAccountCredentials.from_json_keyfile_name('SandCastleDB-778c18763602.json', scope)
client = gspread.authorize(creds)

sheet = client.open('SandCastleHashes').sheet1
hashes = sheet.get_all_records()

def hash_file(path):
    return os.popen("CredUtil -hashfile " + path + " SHA256").read().split("\n")[1]

def does_file_exist(path):
    return (hash_file(path) in hashes)

def upload_hash(path):
    row = [hash_file(path), "FILL_IN_IS_OK"]
    sheet.insert(row, len(hashes))