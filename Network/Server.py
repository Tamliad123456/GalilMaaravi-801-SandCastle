import socket
import base64
import json
import os
import ctypes
import sys
import logging
import check_file as checker
import random
import Encryption

BUFFER_SIZE = 4096
IP = "0.0.0.0"
PORT = 1337
LOG_FILE = "log.txt"

logging.basicConfig(level=logging.DEBUG)

extentions = {"exe" : "1", "dll" : "2", "bat" : "3", "vbs" : "4", "com" : "5", "pyc" : "6", "py" : "7", "else" : "8"}
commands = {"send_file" : "1", "stop_run" : "2", "send_log" : "3", "disconnect" : "4", "le_supriso" : "1337"}
params = {"proceed_to_dynamic" : "1", "force_dynamic" : "2", "skip_static" : "3", "limit_to_static" : "4"}

current_extention = ""

def main():
    '''the main function'''
    
    os.system("title server")
    
    with socket.socket() as sock:
        sock.bind((IP, PORT))
        sock.listen(0)

        while True:
            client, addr = sock.accept()
            #print(exchange_keys(client))
            logging.info("accepted " + str(addr))
            handle_client(client, addr)

    #send_log()

    sock.close()


def send_data(sock, data):
    data = Encryption.encrypt(Encryption.encrypt_key, data)
    sock.sendall(data)
    print("data sent")

def recv_data(sock):
    data = sock.recv(1024)
    data = Encryption.decrypt(Encryption.encrypt_key, data)
    print(data)
    return data.decode()
    #print("recieved data = " + data)

def exchange_keys(sock):
    p = int(recv_data(sock))
    g = int(recv_data(sock))
    a1 = random.randint(1,4500)
    a = (g**a1) % p
    sock.send(bytes(str(a),"ASCII"))
    a2 = int(recv_data(1024))
    return (a2**a1) % p

def handle_client(client, addr):
    '''the function is for handeling the client messages'''
    message = ""
    while "Finished" not in message:
        message += client.recv(1024).decode()
    message = message[:-8]
    message = Encryption.decrypt(Encryption.encrypt_key, message.encode()).decode()

    message = json.loads(message)
    recv_file(message)

    while True:
        message = recv_data(client)
        logging.info(message)
        message = json.loads(message)
        
        if message["command"] == commands["stop_run"]:
            stop_run(message, client)
            break
        elif message["command"] == commands["le_supriso"]:
            #placeholder for easteregg
            pass
        else:
            logging.error("Error! wrong command! how did you do this?")

    disconnect(client)



def disconnect(sock):
    '''the function is for sending a disconnection message'''
    message = {}
    data = "Session has ended. You might want to connect again"
    command = commands["disconnect"]

    message["data"] = data
    message["command"] = command

    send_data(sock, json.dumps(message))

def recv_file(message):
    '''the function is for reciving a new file to run'''
    data = message["data"]

    data = base64.b64decode(data)
    if message["extention"] == "else":
        message["extention"] = "txt"
    print(message["extention"])
    with open("to_run." + find_extention(message["extention"]), "wb") as to_run:    
        to_run.write(data)
    
    if message["extention"] != "txt":
        run_file_as_admin("to_run", find_extention(message["extention"]))


def find_extention(num):
    for x in extentions.keys():
        if extentions[x] == num:
            return x

def run_file_as_admin(filename, extention):
    #ctypes.windll.shell32.ShellExecuteW(None, "runas", sys.executable, filename, None, 1)
    global current_extention
    logging.debug(extention + " " + filename)
    current_extention = extention

    checker.run_check_script(extention, 0)
    #if extention != "py" and "pyc":   
    #    os.system('Powershell -Command "& { Start-Process \"'+ filename + '.' + extention + '\" -Verb RunAs } "')
    #else:
    #    os.system('Powershell -Command "& { Start-Process \"python.exe\" ' + filename + '.' + extention + '\" -Verb RunAs } "')

def send_log(sock):
    with open(LOG_FILE, "r") as log_file:
        data = log_file.read()
    
    message = {"command" : commands["send_log"], "data" : data}
    send_data(sock, json.dumps(message))
    #send_data(sock, ("Finished"))

def stop_run(message, sock):
    os.system("taskkill /f /im to_run." + current_extention)
    send_log(sock)

if __name__ == "__main__":
    main()