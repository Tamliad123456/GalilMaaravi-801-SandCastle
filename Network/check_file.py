#the main script to check the file for bad behavior

import os
import time
from subprocess import check_output
import psutil

def run_check_script(extention, run_order):
    '''function to distribute the task of checking the file'''

    #os.system(r"..\\changeSystemTime\\x64\\Release\\changeSystemTime.exe")
    
    if(run_order == 0):
        errors = run_static(extention)
        return (errors if errors != "NO ERRORS" else run_dynamic(extention))
    elif run_order == 1:
        return run_static(extention)
    elif run_order == 2:
        return run_dynamic(extention)

    return None


def run_static(extention):
    '''function to run the static (yara) analysis on the given file'''

    os.system(r"..\Yara\yara64.exe ..\Yara\rules.yar to_run." + extention + " > yar_output.txt")

    with open("yar_output.txt", 'r') as yar:
        if "to_run" not in yar.readlines():
            return "NO ERRORS"
        return [x for x in yar.readlines() if "to_run" in x]

    os.system(r"..\Yara\yara64.exe ..\Yara\Suspicios.yar to_run." + extention + " > yar_output.txt")

    with open("yar_output.txt", 'r') as yar:
        if "to_run" not in yar.readlines():
            return "NO ERRORS"
        return [x for x in yar.readlines() if "to_run" in x]
    
    return None


def run_dynamic(extention):
    '''function to run the dynamic (with hooks) analysis on the given file'''

    os.system("start to_run." + extention)
    
    #pid = int(check_output(["pidof", "to_run." + extention]))
    path = os.getcwd().replace("\\", "\\\\") + r"\\to_run." + extention
    print(path)
    pid = (os.popen(r"wmic process where ExecutablePath='" + path + r"' get ProcessId").read().split("\n"))
    print(pid)
    pid = int(pid[2].replace(" ", ""))
    p = psutil.Process(pid)
    p.suspend()

    os.system(r"..\Injector\x64\Debug\dllInjection.exe " + pid + r" ..\HookDll\x64\Debug\HookDll.dll")
    os.system(r"wnfdump.exe -n WNF_AUDC_CAPTURE > audioCaptureWNF.txt")

    with open("dynamic_log.txt", 'r') as log:
        #RUN ANALYSIS
        return None

    return None
