import socket, base64, json, os, threading, logging
from run_verification import *


BUFFER_SIZE = 4096
DEFUALT_CONFIG = "config.txt"

logging.basicConfig(level=logging.DEBUG)


#all of these settings will come in the future form the config file and from the settings in the GUI
extentions = {"exe" : "1", "dll" : "2", "bat" : "3", "vbs" : "4", "com" : "5", "pyc" : "6", "py" : "7", "else" : "8"}
commands = {"send_file" : "1", "stop_run" : "2", "send_log" : "3", "disconnect" : "4", "le_supriso" : "1337"}
params = {"proceed_to_dynamic" : "1", "force_dynamic" : "2", "skip_static" : "3", "limit_to_static" : "4"}

last_filename = ""

def find_extensions(file_name):
    filename, extention = os.path.splitext(file_name)
    extention = extention[1:]
    logging.debug(extention + " " + filename)
    
    if extention in extentions:
        return extention
    return "else"


def get_configuration():
    with open(DEFUALT_CONFIG, 'r') as config:
        attributes = config.read()
        try:
            attributes = json.loads(attributes)
            ip = attributes["ip"]
            port = attributes["port"]
            return ip, port
        except:
            logging.error("config file is corrupted")

def main():
    global last_filename
    message = {}
    os.system("title client")
    
    ip, port = get_configuration()
    
            
    filename = input("enter file path: ")
    last_filename = filename

    data = get_file_base64(filename)

#    if os.path.isfile(filename):
#        with open(filename, "rb") as the_file:
#            data = the_file.read()
#            the_file.close()
#    else:
#        raise Exception("File doesnt exists")
#
#    data = base64.b64encode(data).decode()
#

    message["data"] = data
    message["extention"] = extentions[find_extensions(filename)]
    
    message["params"] = params["limit_to_static"] if message["extention"] == extentions["else"] else params["proceed_to_dynamic"]
    
    message["command"] = commands["send_file"]
    
    with socket.socket() as sock:
        sock.connect((ip, port))
        str_message = json.dumps(message)

        sock.send(str_message.encode())
        sock.send(b"Finished")

        t = threading.Thread(target = get_log, args = (sock,))
        t.start()

        finish_check = input("press X if you want to finish: ")
        if finish_check == 'X':
            logging.info("finishing")
            stop_run(sock)
            #t._stop()
        if t.isAlive():
            t.join()

        disconnect(sock)
        logging.info("Ciao!")


def disconnect(sock):
    message = sock.recv(BUFFER_SIZE).decode()
    logging.info(message)
    if message != "":
        message = json.loads(message)
        if message["command"] == commands["disconnect"]:
            sock.close()
            logging.info("Session has ended.\nYou can connect again and send another file!\n\n")

def stop_run(sock):
    global last_filename
    logging.debug(last_filename)
    message = {"command" : commands["stop_run"], "extention" : extentions[last_filename.split('.')[1]]}
    sock.sendall(json.dumps(message).encode())
    #get_log(sock)


def get_log(sock):
    log = ""
    while log == "":
        try:
            log = sock.recv(BUFFER_SIZE).decode()
            logging.debug("the log=" + log)
        except Exception as e:
            #logging.error("exception on log" + e)
            pass

if __name__ == "__main__":
    main()