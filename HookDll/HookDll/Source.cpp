#define _SILENCE_TR1_NAMESPACE_DEPRECATION_WARNING
#define _CRT_SECURE_NO_WARNINGS
#include "Hook.h"
#include <TlHelp32.h>
#include <string> 
MESSAGEBOXA fpMessageBoxA = NULL;
CLOSEHANDLE originalClose = NULL;
OPENFILE originalOpenFile = NULL;
CREATEFILE originalCreateFile = NULL;
OPENPROCESS originalOpenProcess = NULL;
READFILE originalReadFile = NULL;
WRITEFILE originalWriteFile = NULL;
GETKERNELVERSION originalKernelVersion = NULL;
COPYMEMORY originalCopyMemory = NULL;
VIRTUALALLOCHOOK originalVirtualAlloc = NULL;
WRITEPROCESSMEMORYHOOK originalWriteProcessMemory = NULL;
FILE * logfile;


HANDLE threadHandle;

#define BUFSIZE 512




//
//HANDLE GetMainThreadId() {
//	const std::tr1::shared_ptr<void> hThreadSnapshot(
//		CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, 0), CloseHandle);
//	if (hThreadSnapshot.get() == INVALID_HANDLE_VALUE) {
//		throw std::runtime_error("GetMainThreadId failed");
//	}
//	THREADENTRY32 tEntry;
//	tEntry.dwSize = sizeof(THREADENTRY32);
//	DWORD result = 0;
//	DWORD currentPID = GetCurrentProcessId();
//	for (BOOL success = Thread32First(hThreadSnapshot.get(), &tEntry);
//		!result && success && GetLastError() != ERROR_NO_MORE_FILES;
//		success = Thread32Next(hThreadSnapshot.get(), &tEntry))
//	{
//		if (tEntry.th32OwnerProcessID == currentPID) {
//			result = tEntry.th32ThreadID;
//			return hThreadSnapshot.get();
//		}
//	}
//	return hThreadSnapshot.get();
//}


// Pass 0 as the targetProcessId to suspend threads in the current process
void DoSuspendThread(DWORD targetProcessId, DWORD targetThreadId)
{
	HANDLE h = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, 0);
	if (h != INVALID_HANDLE_VALUE)
	{
		THREADENTRY32 te;
		te.dwSize = sizeof(te);
		if (Thread32First(h, &te))
		{
			do
			{
				if (te.dwSize >= FIELD_OFFSET(THREADENTRY32, th32OwnerProcessID) + sizeof(te.th32OwnerProcessID))
				{
					// Suspend all threads EXCEPT the one we want to keep running
					if (te.th32ThreadID != targetThreadId && te.th32OwnerProcessID == targetProcessId)
					{
						HANDLE thread = ::OpenThread(THREAD_ALL_ACCESS, FALSE, te.th32ThreadID);
						printf("got here %d\n", GetThreadId(thread));
						if (thread != NULL)
						{
							SuspendThread(thread);
							CloseHandle(thread);
						}
					}
				}
				te.dwSize = sizeof(te);
			} while (Thread32Next(h, &te));
		}
		CloseHandle(h);
	}
}

// Pass 0 as the targetProcessId to suspend threads in the current process
void DoResumeThread(DWORD targetProcessId, DWORD targetThreadId)
{
	HANDLE h = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, 0);
	if (h != INVALID_HANDLE_VALUE)
	{
		THREADENTRY32 te;
		te.dwSize = sizeof(te);
		if (Thread32First(h, &te))
		{
			do
			{
				if (te.dwSize >= FIELD_OFFSET(THREADENTRY32, th32OwnerProcessID) + sizeof(te.th32OwnerProcessID))
				{
					// Suspend all threads EXCEPT the one we want to keep running
					if (te.th32ThreadID != targetThreadId && te.th32OwnerProcessID == targetProcessId)
					{
						HANDLE thread = ::OpenThread(THREAD_ALL_ACCESS, FALSE, te.th32ThreadID);
						if (thread != NULL)
						{
							ResumeThread(thread);
							CloseHandle(thread);
						}
					}
				}
				te.dwSize = sizeof(te);
			} while (Thread32Next(h, &te));
		}
		CloseHandle(h);
	}
}

std::string GetFileNameFromHandle(HANDLE hFile)
{
	BOOL bSuccess = FALSE;
	TCHAR pszFilename[MAX_PATH + 1];
	HANDLE hFileMap;

	// Get the file size.
	DWORD dwFileSizeHi = 0;
	DWORD dwFileSizeLo = GetFileSize(hFile, &dwFileSizeHi);

	if (dwFileSizeLo == 0 && dwFileSizeHi == 0)
	{
		_tprintf(TEXT("Cannot map a file with a length of zero.\n"));
		return FALSE;
	}

	// Create a file mapping object.
	hFileMap = CreateFileMapping(hFile,
		NULL,
		PAGE_READONLY,
		0,
		1,
		NULL);

	if (hFileMap)
	{
		// Create a file mapping to get the file name.
		void* pMem = MapViewOfFile(hFileMap, FILE_MAP_READ, 0, 0, 1);

		if (pMem)
		{
			if (GetMappedFileName(GetCurrentProcess(),
				pMem,
				pszFilename,
				MAX_PATH))
			{

				// Translate path with device name to drive letters.
				TCHAR szTemp[BUFSIZE];
				szTemp[0] = '\0';

				if (GetLogicalDriveStrings(BUFSIZE - 1, szTemp))
				{
					TCHAR szName[MAX_PATH];
					TCHAR szDrive[3] = TEXT(" :");
					BOOL bFound = FALSE;
					TCHAR* p = szTemp;

					do
					{
						// Copy the drive letter to the template string
						*szDrive = *p;

						// Look up each device name
						if (QueryDosDevice(szDrive, szName, MAX_PATH))
						{
							size_t uNameLen = _tcslen(szName);

							if (uNameLen < MAX_PATH)
							{
								bFound = _tcsnicmp(pszFilename, szName, uNameLen) == 0
									&& *(pszFilename + uNameLen) == _T('\\');

								if (bFound)
								{
									// Reconstruct pszFilename using szTempFile
									// Replace device path with DOS path
									TCHAR szTempFile[MAX_PATH];
									StringCchPrintf(szTempFile,
										MAX_PATH,
										TEXT("%s%s"),
										szDrive,
										pszFilename + uNameLen);
									StringCchCopyN(pszFilename, MAX_PATH + 1, szTempFile, _tcslen(szTempFile));
								}
							}
						}

						// Go to the next NULL character.
						while (*p++);
					} while (!bFound && *p); // end of string
				}
			}
			bSuccess = TRUE;
			UnmapViewOfFile(pMem);
		}

		CloseHandle(hFileMap);
	}

	return bSuccess == TRUE ? pszFilename : "The file name wasnt found";
}

std::string getPermissions(DWORD access)
{
	int newAccess = access;
std::string value = "";
int k = 0, n = newAccess;
for (int c = 31; c >= 0; c--)
{
	k = n >> c;
	if (c == 16)
	{
		if (k & 1)
			value += "DELETE | ";
	}
	if (c == 17)
	{
		if (k & 1)
			value += "READ_CONTROL | ";
	}
	if (c == 18)
	{
		if (k & 1)
			value += "WRITE_DAC | ";
	}
	if (c == 19)
	{
		if (k & 1)
			value += "WRITE_OWNER | ";
	}
	if (c == 20)
	{
		if (k & 1)
			value += "SYNCHRONIZE | ";
	}
	if (c == 28)
	{
		if (k & 1)
			value += "GENERIC_ALL | ";
	}
	if (c == 29)
	{
		if (k & 1)
			value += "GENERIC_EXECUTE | ";
	}
	if (c == 30)
	{
		if (k & 1)
			value += "GENERIC_WRITE | ";
	}
	if (c == 31)
	{
		if (k & 1)
			value += "GENERIC_READ | ";
	}
}

return value;

}

void RtlCopyMemoryHook(void* Destination, void* Source, size_t Length)
{
	fprintf(logfile, std::string("Used kernel memory copy\n").c_str());
	fflush(logfile);

	return originalCopyMemory(Destination, Source, Length);
}

VOID NTAPI RtlGetNtVersionNumbersHook(OUT PULONG pMajorVersion, OUT PULONG pMinorVersion, OUT PULONG pBuildNumber)
{
	fprintf(logfile, std::string("Got Kernel Version\n").c_str());
	fflush(logfile);

	originalKernelVersion(pMajorVersion, pMinorVersion, pBuildNumber);
}

NTSTATUS WINAPI closeHandleHook(HANDLE hObject)
{
	fprintf(logfile, std::string("Closed Handle named " + GetFileNameFromHandle(hObject) + "\n").c_str());
	fflush(logfile);

	return originalClose(hObject);
}

NTSTATUS WINAPI readFileHook(HANDLE FileHandle, HANDLE Event, PIO_APC_ROUTINE ApcRoutine, PVOID ApcContext, PIO_STATUS_BLOCK IoStatusBlock, PVOID Buffer, ULONG Length, PLARGE_INTEGER ByteOffset, PULONG Key)
{
	fprintf(logfile, std::string("Reading from file " + GetFileNameFromHandle(FileHandle) + "\n").c_str());
	fflush(logfile);

	return originalReadFile(FileHandle, Event, ApcRoutine, ApcContext, IoStatusBlock, Buffer, Length, ByteOffset, Key);
}


NTSTATUS WINAPI writeFileHook(
	HANDLE           FileHandle,
	HANDLE           Event,
	PIO_APC_ROUTINE  ApcRoutine,
	PVOID            ApcContext,
	PIO_STATUS_BLOCK IoStatusBlock,
	PVOID            Buffer,
	ULONG            Length,
	PLARGE_INTEGER   ByteOffset,
	PULONG           Key
)
{
	HANDLE foo = (HANDLE)_get_osfhandle(_fileno(logfile));

	NTSTATUS value = originalWriteFile(FileHandle, Event, ApcRoutine, ApcContext, IoStatusBlock, Buffer, Length, ByteOffset, Key);
	if (FileHandle != foo)
	{
		//std::cout << FileHandle << std::endl << foo << std::endl;
		fprintf(logfile, std::string("Writing to file " + GetFileNameFromHandle(FileHandle) + " the values: " + getString((wchar_t*)Buffer) + "\n").c_str());
		fflush(logfile);
	}
	return value;
}


NTSTATUS WINAPI createFileHook(PHANDLE FileHandle, ACCESS_MASK DesiredAccess, POBJECT_ATTRIBUTES ObjectAttributes, PIO_STATUS_BLOCK  IoStatusBlock, PLARGE_INTEGER AllocationSize, ULONG FileAttributes, ULONG ShareAccess, ULONG CreateDisposition, ULONG CreateOptions, PVOID EaBuffer, ULONG EaLength)
{
	
	fprintf(logfile, std::string("The file " + getString(ObjectAttributes->ObjectName->Buffer) + " has been created with " + getPermissions(DesiredAccess) +  " permissions\n").c_str());
	fflush(logfile);

	return originalCreateFile(FileHandle, DesiredAccess, ObjectAttributes, IoStatusBlock, AllocationSize, FileAttributes, ShareAccess, CreateDisposition, CreateOptions, EaBuffer, EaLength);
}


NTSTATUS WINAPI openFileHook(PHANDLE FileHandle, ACCESS_MASK DesiredAccess, POBJECT_ATTRIBUTES ObjectAttributes, PIO_STATUS_BLOCK IoStatusBlock, ULONG ShareAccess, ULONG OpenOptions)
{
	fprintf(logfile, std::string("The file " + getString(ObjectAttributes->ObjectName->Buffer) + " has been opened with" + getPermissions(DesiredAccess) + " permissions\n").c_str());
	fflush(logfile);

	return originalOpenFile(FileHandle, DesiredAccess, ObjectAttributes, IoStatusBlock, ShareAccess, OpenOptions);
}


NTSTATUS WINAPI openProcessHook(PHANDLE ProcessHandle, PHANDLE ThreadHandle, ACCESS_MASK ProcessDesiredAccess, ACCESS_MASK ThreadDesiredAccess, POBJECT_ATTRIBUTES ProcessObjectAttributes, POBJECT_ATTRIBUTES ThreadObjectAttributes, ULONG ProcessFlags, ULONG ThreadFlags, PRTL_USER_PROCESS_PARAMETERS ProcessParameters, PPROCESS_CREATE_INFO CreateInfo, PPROCESS_ATTRIBUTE_LIST AttributeList)
{
	if (getString(ProcessParameters->CommandLine.Buffer).find("DllInjector.exe") == std::string::npos)
	{
		fprintf(logfile, std::string("The process " + getString(ProcessParameters->ImagePathName.Buffer) + " has been opened with params " + getString(ProcessParameters->CommandLine.Buffer) + "\n").c_str());
		std::string base_filename = getString(ProcessParameters->ImagePathName.Buffer).substr(getString(ProcessParameters->ImagePathName.Buffer).find_last_of("/\\") + 1);
		fflush(logfile);
		system(std::string("DllInjector.exe " + base_filename + " HookDll.dll").c_str());//the 'debug' should be 'release' one day
	}
	return originalOpenProcess(ProcessHandle, ThreadHandle, ProcessDesiredAccess, ThreadDesiredAccess, ProcessObjectAttributes, ThreadObjectAttributes, ProcessFlags, ThreadFlags, ProcessParameters, CreateInfo, AttributeList);
}



 LPVOID WINAPI hookVirtualAlloc(HANDLE hProcess, LPVOID lpAddress, SIZE_T dwSize, DWORD flAllocationType, DWORD flProtect)
{
	 fprintf(logfile, std::string("used virtualallocex").c_str());
	 fflush(logfile);
	 return originalVirtualAlloc(hProcess, lpAddress, dwSize, flAllocationType, flProtect);
}

 BOOL WINAPI WriteProcessMemoryHook(
	 HANDLE  hProcess,
	 LPVOID  lpBaseAddress,
	 LPCVOID lpBuffer,
	 SIZE_T  nSize,
	 SIZE_T* lpNumberOfBytesWritten
 )
 {
	 fprintf(logfile, std::string("used writeprocessmemory").c_str());
	 fflush(logfile);
	 return originalWriteProcessMemory(hProcess, lpBaseAddress, lpBuffer, nSize, lpNumberOfBytesWritten);
 }

std::string getString(wchar_t* buffer)
{
	std::string value = "";
	int i = 0;
	while (buffer[i] != 0)
	{
		value += buffer[i];
		i++;
	}
	return value;
}


DWORD WINAPI HandleHooks(LPVOID lpParams)
{
	if ((logfile = fopen("dynamic.log", "a")) != NULL)
	{
		fprintf(logfile, "starting log\n");
	}

	if (MH_Initialize() != MH_OK)
	{
		std::cout << "hook isnt initialized" << std::endl;
		return 1;
	}


	args* createFileArgs = getArgs(&createFileHook, &originalCreateFile, "NtDll.dll", "NtCreateFile");
	CreateThread(NULL, NULL, &createAndEnableHook, ((LPVOID*)createFileArgs), NULL, NULL);

	args* openFileArgs = getArgs(&openFileHook, &originalOpenFile, "NtDll.dll", "NtOpenFile");
	CreateThread(NULL, NULL, &createAndEnableHook, ((LPVOID*)openFileArgs), NULL, NULL);

	args* createProcessArgs = getArgs(&openProcessHook, &originalOpenProcess, "NtDll.dll", "ZwCreateUserProcess");
	CreateThread(NULL, NULL, &createAndEnableHook, ((LPVOID*)createProcessArgs), NULL, NULL);

	//createAndEnableHook(&closeHandleHook, originalClose, "NtDll.dll", "NtClose"); //was cut-out because it crashed the process injected to
	args* writeFileArgs = getArgs(&writeFileHook, &originalWriteFile, "NtDll.dll", "NtWriteFile");
	CreateThread(NULL, NULL, &createAndEnableHook, ((LPVOID*)writeFileArgs), NULL, NULL);

	args* readFileArgs = getArgs(&readFileHook, &originalReadFile, "NtDll.dll", "NtReadFile");
	CreateThread(NULL, NULL, &createAndEnableHook, ((LPVOID*)readFileArgs), NULL, NULL);

	//args* kernelMemoryCopy = getArgs(&RtlCopyMemoryHook, &originalCopyMemory, "NtDll.dll", "RtlCopyMemory"); no need
	//CreateThread(NULL, NULL, &createAndEnableHook, ((LPVOID*)kernelMemoryCopy), NULL, NULL);

	args* getKernelVersionArgs = getArgs(&RtlGetNtVersionNumbersHook, &originalKernelVersion, "NtDll.dll", "RtlGetNtVersionNumbers");
	CreateThread(NULL, NULL, &createAndEnableHook, ((LPVOID*)getKernelVersionArgs), NULL, NULL);


	args* virtualAllocArgs = getArgs(&hookVirtualAlloc, &originalVirtualAlloc, "Kernel32.dll", "VirtualAllocEx");
	CreateThread(NULL, NULL, &createAndEnableHook, ((LPVOID*)virtualAllocArgs), NULL, NULL);

	args* WriteProcessMemoryArgs = getArgs(&WriteProcessMemoryHook, &originalWriteProcessMemory, "Kernel32.dll", "WriteProcessMemory");
	CreateThread(NULL, NULL, &createAndEnableHook, ((LPVOID*)WriteProcessMemoryArgs), NULL, NULL);

	return 0;
	
}


args* getArgs(LPVOID hookedFunction, LPVOID theOriginalFunction, LPCSTR moduleName, LPCSTR function)
{
	args* arg = new args();
	arg->function = function;
	arg->hookedFunction = hookedFunction;
	arg->theOriginalFunction = theOriginalFunction;
	arg->moduleName = moduleName;
	return arg;
}

DWORD createAndEnableHook(LPVOID func)//LPVOID hookedFunction, LPVOID theOriginalFunction, LPCSTR moduleName, LPCSTR function)
{
	args* arguments = (args*)func;
	LPVOID hookedFunction = arguments->hookedFunction;
	LPVOID theOriginalFunction = arguments->theOriginalFunction;
	LPCSTR moduleName = arguments->moduleName;
	LPCSTR function = arguments->function;
	
	if (MH_CreateHook(GetProcAddress(GetModuleHandle(moduleName), function), hookedFunction,
		reinterpret_cast<LPVOID*>(theOriginalFunction)) != MH_OK)
	{
		MessageBoxA(NULL, (std::string("Unable to create hook to function ") + std::string(function)).c_str(), "Hook Error", MB_OK);
		return 1;
	}

	if (MH_EnableHook(GetProcAddress(GetModuleHandle(moduleName), function)) != MH_OK)
	{
		MessageBoxA(NULL, (std::string("Unable to enable hook to function ") + std::string(function)).c_str(), "Hook Error", MB_OK);
		return 1;
	}
	return 0;
}



DWORD DisableHookByFunction(LPVOID function)
{
	if (MH_DisableHook(function) != MH_OK)
	{
		return 1;
	}

	return 0;
}


BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{

	switch (fdwReason)
	{
		case DLL_PROCESS_ATTACH:
			DoSuspendThread(GetCurrentProcessId(), GetCurrentThreadId());
			printf("suspended\n");
			CreateThread(NULL, NULL, HandleHooks, NULL, NULL, NULL);
			printf("Resuming now\n");
			DoResumeThread(GetCurrentProcessId(), GetCurrentThreadId());

			break;
		case DLL_PROCESS_DETACH:
			break;
		default:
			break;
	}
	return TRUE;
}
//check hooks