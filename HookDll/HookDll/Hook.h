#pragma once
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <io.h>
#include <iostream>
#include <tchar.h>
#include <string.h>
#include <psapi.h>
#include <strsafe.h>
#include <winternl.h>
#include <fstream>
#include "MinHook.h"
#include <map>
#pragma comment(lib, "NtDll.lib")
#pragma comment(lib, "libMinHook.lib")

typedef void PROCESS_CREATE_INFO, *PPROCESS_CREATE_INFO;
typedef void PROCESS_ATTRIBUTE_LIST, *PPROCESS_ATTRIBUTE_LIST;


typedef struct {
	//Or whatever information that you need
	LPVOID hookedFunction;
	LPVOID theOriginalFunction;
	LPCSTR moduleName;
	LPCSTR function;
} args;


typedef int (WINAPI* MESSAGEBOXA)(HWND, LPCSTR, LPCSTR, UINT);
//typedef BOOL(WINAPI* CLOSEHANDLE)(HANDLE hObject); // close file / process
typedef NTSTATUS(WINAPI* CREATEFILE)(PHANDLE FileHandle, ACCESS_MASK DesiredAccess, POBJECT_ATTRIBUTES ObjectAttributes, PIO_STATUS_BLOCK  IoStatusBlock, PLARGE_INTEGER AllocationSize, ULONG FileAttributes, ULONG ShareAccess, ULONG CreateDisposition, ULONG CreateOptions, PVOID EaBuffer, ULONG EaLength);
typedef NTSTATUS(WINAPI* OPENFILE)(PHANDLE FileHandle, ACCESS_MASK DesiredAccess, POBJECT_ATTRIBUTES ObjectAttributes, PIO_STATUS_BLOCK IoStatusBlock, ULONG ShareAccess, ULONG OpenOptions);
typedef NTSTATUS(WINAPI* OPENPROCESS)(PHANDLE ProcessHandle, PHANDLE ThreadHandle, ACCESS_MASK ProcessDesiredAccess, ACCESS_MASK ThreadDesiredAccess, POBJECT_ATTRIBUTES ProcessObjectAttributes, POBJECT_ATTRIBUTES ThreadObjectAttributes, ULONG ProcessFlags, ULONG ThreadFlags, PRTL_USER_PROCESS_PARAMETERS ProcessParameters, PPROCESS_CREATE_INFO CreateInfo, PPROCESS_ATTRIBUTE_LIST AttributeList);
typedef NTSTATUS(WINAPI* CLOSEHANDLE)(HANDLE Handle);
typedef NTSTATUS(WINAPI* READFILE)(HANDLE FileHandle, HANDLE Event, PIO_APC_ROUTINE ApcRoutine, PVOID ApcContext, PIO_STATUS_BLOCK IoStatusBlock, PVOID Buffer, ULONG Length, PLARGE_INTEGER ByteOffset, PULONG Key);
//typedef BOOL(WINAPI* WRITEFILE)(HANDLE hFile, LPCVOID lpBuffer, DWORD nNumberOfBytesToWrite, LPDWORD lpNumberOfBytesWritten, LPOVERLAPPED lpOverlapped);
typedef void (WINAPI* COPYMEMORY)(void* Destination, void* Source, size_t Length);

//typedef VOID(WINAPI* GETKERNELVERSION)(DWORD* major, DWORD* minor, DWORD* build);
typedef VOID(NTAPI* GETKERNELVERSION)(OUT PULONG pMajorVersion, OUT PULONG pMinorVersion, OUT PULONG pBuildNumber);

typedef LPVOID (WINAPI* VIRTUALALLOCHOOK)(HANDLE hProcess,LPVOID lpAddress,SIZE_T dwSize,DWORD flAllocationType,DWORD flProtect);
typedef  BOOL(WINAPI* WRITEPROCESSMEMORYHOOK)(
	HANDLE  hProcess,
	LPVOID  lpBaseAddress,
	LPCVOID lpBuffer,
	SIZE_T  nSize,
	SIZE_T* lpNumberOfBytesWritten
	);
typedef NTSTATUS(WINAPI* WRITEFILE)(HANDLE FileHandle,HANDLE Event,PIO_APC_ROUTINE ApcRoutine,PVOID ApcContext,PIO_STATUS_BLOCK IoStatusBlock, PVOID Buffer, ULONG Length, PLARGE_INTEGER ByteOffset, PULONG Key);
//functions
DWORD createAndEnableHook(LPVOID args);
args* getArgs(LPVOID hookedFunction, LPVOID theOriginalFunction, LPCSTR moduleName, LPCSTR function);
DWORD WINAPI HandleHooks(LPVOID lpParams);
//DWORD WINAPI DisableHooks(LPVOID lpParam);
DWORD DisableHookByFunction(LPVOID function);
NTSTATUS WINAPI openFileHook(PHANDLE FileHandle, ACCESS_MASK DesiredAccess, POBJECT_ATTRIBUTES ObjectAttributes, PIO_STATUS_BLOCK IoStatusBlock, ULONG ShareAccess, ULONG OpenOptions);
NTSTATUS WINAPI createFileHook(PHANDLE FileHandle, ACCESS_MASK DesiredAccess, POBJECT_ATTRIBUTES ObjectAttributes, PIO_STATUS_BLOCK  IoStatusBlock, PLARGE_INTEGER AllocationSize, ULONG FileAttributes, ULONG ShareAccess, ULONG CreateDisposition, ULONG CreateOptions, PVOID EaBuffer, ULONG EaLength);
NTSTATUS WINAPI closeHandleHook(HANDLE Handle);
std::string getString(wchar_t* buffer);
NTSTATUS WINAPI readFileHook(HANDLE FileHandle, HANDLE Event, PIO_APC_ROUTINE ApcRoutine, PVOID ApcContext, PIO_STATUS_BLOCK IoStatusBlock, PVOID Buffer, ULONG Length, PLARGE_INTEGER ByteOffset, PULONG Key);
//VOID WINAPI getKernelVersionHook(DWORD* major, DWORD* minor, DWORD* build);


VOID NTAPI RtlGetNtVersionNumbersHook(OUT PULONG pMajorVersion, OUT PULONG pMinorVersion, OUT PULONG pBuildNumber);
void RtlCopyMemoryHook(void* Destination,void* Source,size_t Length);
//check hooks
