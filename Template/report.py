import sys
import datetime
import json

def main():
    '''the main function'''
    if len(sys.argv) != 6:
        print("""wrong params:
				 1)file name
				 2)file hash
				 3)explenation
				 4)virus total
				 5)log file""")
    else:
        file_name = sys.argv[1]
        file_hash = sys.argv[2]
        explenation = sys.argv[3]
        virus_total = sys.argv[4]
        log_file = sys.argv[5]

        status = ""

        log_data = open(log_file, "r").read()
        log_data = log_data.replace('\\n', '\n')

        with open("analysisOutput.txt", 'r') as output:
        	level = json.loads(output.read())
        	if level["level"] == "0":
        		status = "ok"
        	elif level["level"] == "1":
        		status = "suspicous"
        	else:
        		status = "bad"

        with open("template.html", "r") as t:
            template_data = t.read()
            template_data = template_data.replace("{{FILE_NAME}}", file_name)
            template_data = template_data.replace("{{HASH}}", file_hash)
            template_data = template_data.replace("{{STATUS}}", status)
            template_data = template_data.replace("{{SHORT_EXPLENATION}}", explenation + "<br>" + virus_total)
            template_data = template_data.replace('name="log_file">', 'name="log_file">' + log_data)
            template_data = template_data.replace("{{TIME}}", str(f'{datetime.datetime.now():%Y-%m-%d %H:%M}'))
            with open("SandCastleOutput.html", "w") as f:
                f.write(template_data)

main()

