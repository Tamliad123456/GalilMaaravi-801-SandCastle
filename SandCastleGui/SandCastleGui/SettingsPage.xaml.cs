﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Core;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace SandCastleGui
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SettingsPage : Page
    {
        public SettingsPage()
        {
            this.InitializeComponent();
            var currentView = SystemNavigationManager.GetForCurrentView();
            currentView.AppViewBackButtonVisibility = AppViewBackButtonVisibility.Visible;
            currentView.BackRequested += backHandler;
            getValues();


        }

        private async void getValues()
        {
            Windows.Storage.StorageFolder storageFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
            Windows.Storage.StorageFile file = await storageFolder.CreateFileAsync("config.cfg", Windows.Storage.CreationCollisionOption.OpenIfExists);
            string text = await Windows.Storage.FileIO.ReadTextAsync(file);
            if(text == "")
            {
                text = "Dynamic=True\nStatic=True";
                await Windows.Storage.FileIO.WriteTextAsync(file, text);
            }
            string[] arr = text.Split('\n');
            
            for(int i = 0; i < 2; i++)
            {
                if (arr[i].Contains("Dynamic"))
                {
                    if(arr[i].Substring(8, 4) == "True")
                    {
                        DynamicCheck.IsChecked = true;
                    }
                    else if(arr[i].Substring(8, 4) == "False")
                    {
                        DynamicCheck.IsChecked = false;
                    }
                }
                else if (arr[i].Contains("Static"))
                {
                    if (arr[i].Substring(7, 4) == "True")
                    {
                        StaticCheck.IsChecked = true;
                    }
                    else if (arr[i].Substring(7, 4) == "False")
                    {
                        StaticCheck.IsChecked = false;
                    }
                }
            }

        }

        private void backHandler(object sender, BackRequestedEventArgs a)
        {
            this.Frame.Navigate(typeof(MainPage));
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            string text = "";

            Windows.Storage.StorageFolder storageFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
            Windows.Storage.StorageFile file = await storageFolder.CreateFileAsync("config.cfg", Windows.Storage.CreationCollisionOption.OpenIfExists);

            if (DynamicCheck.IsChecked == true)
            {
                text = "Dynamic=True\n";
            }
            else
            {
                text = "Dynamic=False\n";
            }
            if (StaticCheck.IsChecked == true)
            {
                text += "Static=True";
            }
            else
            {
                text += "Static=False";
            }

            await Windows.Storage.FileIO.WriteTextAsync(file, text);
        }
    }
}
