#include <Windows.h>
#include <iostream>
#include <thread>
#include <chrono>


/*
taken from msdn for cheking time

also the program should run at admin to work

typedef struct _SYSTEMTIME {
  WORD wYear;
  WORD wMonth;
  WORD wDayOfWeek;
  WORD wDay;
  WORD wHour;
  WORD wMinute;
  WORD wSecond;
  WORD wMilliseconds;
} SYSTEMTIME, *PSYSTEMTIME;
*/

void updateByMonth(SYSTEMTIME* t, int days);
void updateTime(SYSTEMTIME* t);
unsigned int microseconds = 10;

void updateTime(SYSTEMTIME* t)
{
	switch (t->wMonth)
	{
	case 1:
	case 4:
	case 6:
	case 8:
	case 10:
	case 12:
		updateByMonth(t, 31);
		break;

	case 3:
	case 5:
	case 7:
	case 9:
	case 11:
		updateByMonth(t, 30);
		break;

	default:
		/*Fab code*/
		if (t->wYear % 4 == 0)
		{
			updateByMonth(t, 29);
		}
		else
		{
			updateByMonth(t, 28);
		}
		break;
	}
}

void updateByMonth(SYSTEMTIME* t, int days)
{
	if (t->wMinute == 59)
	{
		if (t->wHour == 23)
		{
			t->wMinute = 0;
			t->wHour = 0;

			if (t->wDay == days)
			{
				t->wDay = 0;
			}
			t->wDay++;
		}
		else
		{
			t->wHour++;
			t->wMinute = 0;
		}
	}
	else
	{
		t->wMinute++;
	}
}

int main()
{
	SYSTEMTIME time;
	GetSystemTime(&time);

	while (true)
	{
		updateTime(&time);

		SetSystemTime(&time);
		std::this_thread::sleep_for(std::chrono::milliseconds(microseconds)); // added for balancing the time else windows would crash
	}
	return 0;
}