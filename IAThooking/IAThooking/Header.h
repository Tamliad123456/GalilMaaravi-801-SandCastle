#ifndef _DLL_H_
#define _DLL_H_

#include <windows.h>
#include<conio.h>
#include<iostream>
#include<string>
#include <winternl.h>
//#pragma comment(lib, "NtDll.lib")


//#define TARGET_FUNCTION "lstrcmpA"
#define TARGET_FUNCTION "NtCreateFile"
#define IMPORT_TABLE_OFFSET 1
using namespace std;

DWORD sourceAddr;
//int newlstrcmpA();
//int WINAPI newlstrcmpA(LPCSTR a,LPCSTR b);
bool IAThooking(HMODULE, LPCSTR, PVOID);
bool rewriteThunk(PIMAGE_THUNK_DATA pThunk, void* newFunc);
PIMAGE_IMPORT_DESCRIPTOR getImportTable(HMODULE);
BOOL APIENTRY DllMain(HINSTANCE hInst, DWORD reason, LPVOID reserved);
NTSTATUS WINAPI createFileHook(PHANDLE FileHandle, ACCESS_MASK DesiredAccess, POBJECT_ATTRIBUTES ObjectAttributes, PIO_STATUS_BLOCK  IoStatusBlock, PLARGE_INTEGER AllocationSize, ULONG FileAttributes, ULONG ShareAccess, ULONG CreateDisposition, ULONG CreateOptions, PVOID EaBuffer, ULONG EaLength);
typedef NTSTATUS(WINAPI* OPENFILE)(PHANDLE FileHandle, ACCESS_MASK DesiredAccess, POBJECT_ATTRIBUTES ObjectAttributes, PIO_STATUS_BLOCK IoStatusBlock, ULONG ShareAccess, ULONG OpenOptions);
typedef NTSTATUS (WINAPI* CREATEFILE)(PHANDLE FileHandle, ACCESS_MASK DesiredAccess, POBJECT_ATTRIBUTES ObjectAttributes, PIO_STATUS_BLOCK  IoStatusBlock, PLARGE_INTEGER AllocationSize, ULONG FileAttributes, ULONG ShareAccess, ULONG CreateDisposition, ULONG CreateOptions, PVOID EaBuffer, ULONG EaLength);

#endif /* _DLL_H_ */