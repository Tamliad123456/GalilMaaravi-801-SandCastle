# SandCastle

![SandCastle Banner](https://drive.google.com/uc?export=view&id=1_GkBl4FvfXJLdMBHJTkD_GkqHbUViMKt)

The 'SandCastle' project is an adaptive sandbox submitted as the final project
for the Magshimim cyber program.

SandCastle offers both a static and dynamic analysis for EXE files on a safe and isolated environment.



![Magshimim Logo](https://drive.google.com/uc?export=view&id=1fKgRCdkd42SjmdGvrkeb8U6IPNOMSfVG)


## Installation

The installation of the SandCastle is a rather simple process,
download the SandCastle folder and using python install all of the dependencies as follows.

```cmd
pip install -r ./requirements.txt
```

The project is now on your local machine.


## Local Setup

### Enabling Hyper-V
1. Right click on the Windows button and select 'Apps and Features'.
2. Select Programs and Features on the right under related settings.
3. Select turn windows features on or off.
4. Select Hyper-V and click OK.

### Setting Up Hyper-V
1. Download the Windows 10 image.
2. Setup Windows 10, and follow the steps on "Disabling UAC".
3. Copy the server files into the startup programs folder (instruction can be found at "SandCastle Installation Instructions.docx" in the main repo folder).
4. Exit the Virtual Machine.
5. Create a snapshot called "snapshot1" in the Hyper-V Manager.

### Disabling UAC
1. Launch the Hyper-V Windows image.
2. Open the start menu, and type "Change User Account Control settings".
3. Drag the slider all the way to the bottom.
4. Press OK.

Now we created the isolated environment.
Let's run the program!


## Program Launch
In the /SandCastle/Release folder, find SandCastleGui.exe and run it.
That's it! You're up and running.

## Usage
Open, the program, insert a file (with the directory navigator or drag and drop) and sand it to an automatic analysis on the server.
The results will be displayed when finished.

## Useful Links
Link to the project's architecture - 
```
https://app.moqups.com/zivdrukker@gmail.com/RHG2LgAWwv/edit/page/aa9df7b72
```
Link to the project's specification file - 
```
https://docs.google.com/document/d/1cUmd7Z4eUGMcHINhIsR3wBjF-_PsiZUb0SVlwdSl3g0/edit?usp=sharing
```

## Contact Info
For any questions regarding the repository, contact tamliad123456@gmail.com or zivdrukker@gmail.com.

## License
[MIT](https://choosealicense.com/licenses/mit/)

#
It won't be a hassle with SandCastle

![SandCastle Logo](https://drive.google.com/uc?export=view&id=1-6rBT1r3Aq7Uft8CO3M1iC1hhk6TApQ2)

Yours,                                     
T&Z