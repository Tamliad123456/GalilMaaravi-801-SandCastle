import requests


URL = "https://docs.microsoft.com/en-us/windows/desktop/api/winternl/nf-winternl-"

name = input("enter function name: ")
removeINorOut = True
newUrl = URL + name
print(newUrl)

r = requests.get(newUrl)
data = r.text[r.text.index('<code class="lang-cpp">') + len('<code class="lang-cpp">') :r.text.index('</code>')]

data = data.split("\n")

sign = ""

count = 0

for line in data:
    if removeINorOut:
        line = line.replace("IN", "")
        line = line.replace("OUT", "")
    line = ' '.join(line.split())
    if count == 0 or count == 1 or count == len(data) - 2:
        sign += line
    else:
        sign += " " + line
    count += 1

print("\n" + sign)
