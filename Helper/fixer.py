import sys
file = open(sys.argv[1], "r")
sign = ""
data = file.readlines()
file.close()
count = 0

for line in data:
    if True:
        line = line.replace("IN", "")
        line = line.replace("OUT", "")
    line = ' '.join(line.split())
    if count == 0 or count == 1 or count == len(data) - 2:
        sign += line
    else:
        sign += " " + line
    count += 1

print("\n" + sign)
