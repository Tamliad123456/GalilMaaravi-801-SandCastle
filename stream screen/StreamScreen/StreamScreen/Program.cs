using System;
using System.Text;
using System.Runtime.InteropServices;
using EventHook;
using System.Net.Sockets;
using System.Diagnostics;
using System.Windows.Forms;

namespace StreamScreen
{
    class Program
    {
        private static TcpClient clientSocket;
        static void Main(string[] args)
        {

            clientSocket = new TcpClient();
            Console.WriteLine("enter ip");

            string ip = args[0];
            
            //string ip = Console.ReadLine();

            Process.Start($"http://{ip}:5000/video_feed");
            clientSocket.Connect(ip, 5858);
            Console.WriteLine("connected");

            using (var eventHookFactory = new EventHookFactory())
            {
                var keyboardWatcher = eventHookFactory.GetKeyboardWatcher();
                keyboardWatcher.Start();
                keyboardWatcher.OnKeyInput += (s, e) =>
                {
                    if (GetActiveWindow().Contains("video_feed") && e.KeyData.EventType.ToString() == "down")
                    {
                        Console.WriteLine(string.Format("Key {0} event of key {1}", e.KeyData.EventType, e.KeyData.Keyname));
                        string key = e.KeyData.Keyname.ToString();
                        if (key.Length == 1)
                            sendKeyboard(key);
                        else if (key == "Space")
                            sendKeyboard(" ");
                        else if (key == "Tab")
                            sendKeyboard("TAB");
                        else if (key == "Back")
                            sendKeyboard("BACKSPACE");
                        else if (key == "Delete")
                            sendKeyboard("DEL");
                    }
                };

                var mouseWatcher = eventHookFactory.GetMouseWatcher();
                mouseWatcher.Start();
                mouseWatcher.OnMouseInput += (s, e) =>
                {
                    if (GetActiveWindow().Contains("video_feed") && e.Message.ToString() == "WM_LBUTTONDOWN" || e.Message.ToString() == "WM_RBUTTONDOWN")
                    {
                        if (e.Message.ToString() == "WM_LBUTTONDOWN")
                            SendXAndY(e.Point.x, e.Point.y, 0);
                        else
                            SendXAndY(e.Point.x, e.Point.y, 1);
                        Console.WriteLine(string.Format("Mouse event {0} at point {1},{2}", e.Message.ToString(), e.Point.x, e.Point.y));
                    }
                };

                var clipboardWatcher = eventHookFactory.GetClipboardWatcher();
                clipboardWatcher.Start();
                clipboardWatcher.OnClipboardModified += (s, e) =>
                {
                    if (GetActiveWindow().Contains("video_feed"))
                        Console.WriteLine(string.Format("Clipboard updated with data '{0}' of format {1}", e.Data, e.DataFormat.ToString()));
                };


                var applicationWatcher = eventHookFactory.GetApplicationWatcher();
                applicationWatcher.Start();
                applicationWatcher.OnApplicationWindowChange += (s, e) =>
                {
                    if (GetActiveWindow().Contains("video_feed"))
                        Console.WriteLine(string.Format("Application window of '{0}' with the title '{1}' was {2}", e.ApplicationData.AppName, e.ApplicationData.AppTitle, e.Event));
                };

                var printWatcher = eventHookFactory.GetPrintWatcher();
                printWatcher.Start();
                printWatcher.OnPrintEvent += (s, e) =>
                {
                    if (GetActiveWindow().Contains("video_feed"))
                        Console.WriteLine(string.Format("Printer '{0}' currently printing {1} pages.", e.EventData.PrinterName, e.EventData.Pages));
                };

                Console.Read();

                keyboardWatcher.Stop();
                mouseWatcher.Stop();
                clipboardWatcher.Stop();
                applicationWatcher.Stop();
                printWatcher.Stop();
            }
            Console.ReadKey();

        }

        private static void sendKeyboard(string key)
        {
            NetworkStream serverStream = clientSocket.GetStream();
            byte[] outStream = Encoding.ASCII.GetBytes("0," + key);
            serverStream.Write(outStream, 0, outStream.Length);
            serverStream.Flush();
        }


        private static void SendXAndY(int x, int y, int type)
        {
            NetworkStream serverStream = clientSocket.GetStream();
            byte[] outStream = Encoding.ASCII.GetBytes("1," + x.ToString() + ',' + y.ToString() + ',' + type);
            serverStream.Write(outStream, 0, outStream.Length);
            serverStream.Flush();
        }

        [DllImport("user32.dll")]
        static extern IntPtr GetForegroundWindow();
        [DllImport("user32.dll")]
        static extern int GetWindowText(IntPtr hWnd, StringBuilder text, int count);
        [DllImport("User32.dll")]
        static extern int SetForegroundWindow(IntPtr point);


        private static string GetActiveWindow()
        {
            const int nChars = 256;
            IntPtr handle;
            StringBuilder Buff = new StringBuilder(nChars);
            handle = GetForegroundWindow();
            if (GetWindowText(handle, Buff, nChars) > 0)
            {
                return Buff.ToString();
            }
            return "";
        }

        private static void connect()
        {
            TcpClient client = new TcpClient();
            client.Connect("10.20.1.17", 5858);

            NetworkStream stream = client.GetStream();
            stream.Write(Encoding.ASCII.GetBytes("hook shit"), 0, 0);//insert this line into the hook

            SendKeys.SendWait("F");//has to be SendWait

            Process p = Process.GetProcessesByName("to_run.exe")[0];//we can get it by handle also
            IntPtr pointer = p.Handle;
            SetForegroundWindow(pointer);
            SendKeys.SendWait("F");
        }

    }
}

///////////////////////////////////////////////////////////////////////
///!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!///
///////////////////////////////////////////////////////////////////////

//using System;
//using System.Diagnostics;
//using System.Runtime.InteropServices;
//using System.Windows.Forms;
//using System.Net;
//using System.Net.Sockets;
//using System.Text;

//namespace keyHooks
//{
//    class Program
//    {
//        private const int WH_KEYBOARD_LL = 13;

//        private const int WM_KEYDOWN = 0x0100;
//        private const int WM_SYSKEYDOWN = 0x0104;

//        private static LowLevelKeyboardProc _proc = HookCallback;

//        private static IntPtr _hookID = IntPtr.Zero;
//        private static NetworkStream s;

//        public static void Main()
//        {

//            s = connect();

//            _hookID = SetHook(_proc);

//            Application.Run();

//            UnhookWindowsHookEx(_hookID);

//        }


//        private static IntPtr SetHook(LowLevelKeyboardProc proc)
//        {
//            using (Process curProcess = Process.GetCurrentProcess())
//            using (ProcessModule curModule = curProcess.MainModule)
//            {

//                return SetWindowsHookEx(WH_KEYBOARD_LL, proc,
//                    GetModuleHandle(curModule.ModuleName), 0);

//            }

//        }

//        private delegate IntPtr LowLevelKeyboardProc(int nCode, IntPtr wParam, IntPtr lParam);


//        private static IntPtr PerformType(int nCode, IntPtr wParam, IntPtr lParam)
//        {
//            if (nCode >= 0 && wParam == (IntPtr)WM_KEYDOWN)
//            {

//                int vkCode = Marshal.ReadInt32(lParam);

//                Console.WriteLine((Keys)vkCode);
//            }
//            return CallNextHookEx(_hookID, nCode, wParam, lParam);
//        }

//        private static IntPtr HookCallback(int nCode, IntPtr wParam, IntPtr lParam)
//        {

//            if (nCode >= 0 && wParam == (IntPtr)WM_KEYDOWN)
//            {

//                int vkCode = Marshal.ReadInt32(lParam);

//                Console.WriteLine((Keys)vkCode);
//                sendData(s, ((Keys)vkCode).ToString());

//            }

//            if (nCode >= 0 && wParam == (IntPtr)WM_SYSKEYDOWN)
//            {

//                int vkCode = Marshal.ReadInt32(lParam);

//                Console.WriteLine((Keys)vkCode);
//                sendData(s, ((Keys)vkCode).ToString());


//            }

//            return CallNextHookEx(_hookID, nCode, wParam, lParam);
//        }


//        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]

//        private static extern IntPtr SetWindowsHookEx(int idHook,
//            LowLevelKeyboardProc lpfn, IntPtr hMod, uint dwThreadId);


//        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
//        [return: MarshalAs(UnmanagedType.Bool)]

//        private static extern bool UnhookWindowsHookEx(IntPtr hhk);


//        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]

//        private static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode,
//            IntPtr wParam, IntPtr lParam);


//        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]

//        private static extern IntPtr GetModuleHandle(string lpModuleName);

//        [DllImport("User32.dll")]
//        static extern int SetForegroundWindow(IntPtr point);

//        private static NetworkStream connect()
//        {
//            TcpClient client = new TcpClient();
//            client.Connect("192.168.127.226", 5858);

//            NetworkStream stream = client.GetStream();

//            return stream;
//        }

//        private static void sendData(NetworkStream stream, string text)
//        {
//            stream.Write(Encoding.ASCII.GetBytes(text), 0, 0);//insert this line into the hook
//        }
//    }
//}