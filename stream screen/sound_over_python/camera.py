import cv2
from PIL import ImageGrab
import numpy as np


class VideoCamera(object):
    def __init__(self):
        self.video = cv2.VideoCapture(0)

    def __del__(self):
        self.video.release()



    def get_frame(self,flag):
        try:
            if flag:
             success, image = self.video.read()
            else:
             imgnp = ImageGrab.grab()
             image = np.array(imgnp)
             image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            ret, jpeg = cv2.imencode('.jpg', image)
            return jpeg.tobytes()
        except Exception as e:
            print(e)
