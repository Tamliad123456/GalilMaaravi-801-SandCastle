#import sys
from flask import Flask, render_template, Response
#from camera import VideoCamera
#import os
from threading import Thread
import cv2
from PIL import ImageGrab
import numpy as np
#flag = sys.argv[1]
#if flag == "camera":
#	flag = True
#else:
#	flag = False

flag = False

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

def gen(camera):
    while True:
        #frame = camera.get_frame(flag)
        frame = getFrame()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')

def getFrame():
        imgnp = ImageGrab.grab()
        image = np.array(imgnp)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        ret, jpeg = cv2.imencode('.jpg', image)
        return jpeg.tobytes()



class VideoCamera(object):
    def __init__(self):
        self.video = cv2.VideoCapture(0)

    def __del__(self):
        self.video.release()


@app.route('/video_feed')
def video_feed():
    return Response(gen(VideoCamera()), mimetype='multipart/x-mixed-replace; boundary=frame')

if __name__ == '__main__':
	#t1 = Thread(target = app.run, args = ('0.0.0.0',5000,True))
	#t1.start()
	app.run(host='0.0.0.0', debug=True, threaded=True )