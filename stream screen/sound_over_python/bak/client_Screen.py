# -*- coding: utf-8 -*-
from win10toast import ToastNotifier
toaster = ToastNotifier()
import tkinter
from tkinter import *
from tkinter import ttk
from PIL import Image
import socket
import threading
import sys
import pickle
from PIL import ImageGrab
import time
import os 
from tkinter import tix as tk


class Client():
	#"""docstring for Cliente"""

	def on_entry_click(self,event):
		"""function that gets called whenever entry is clicked"""
		if self.NameText.get() == 'enter your name':
			self.NameText.delete(0, "end") # delete all the text in the entry
			self.NameText.insert(0, '') #Insert blank for user input
			self.NameText.config(fg = 'black')


	def on_focusout(self,event):
		if self.NameText.get() == '':
			self.NameText.insert(0, 'enter your name')
			self.NameText.config(fg = 'grey')


	def on_entry_clickIp(self,event):
		"""function that gets called whenever entry is clicked"""
		if self.IpText.get() == 'enter ip to connect':
			self.IpText.delete(0, "end") # delete all the text in the entry
			self.IpText.insert(0, '') #Insert blank for user input
			self.IpText.config(fg = 'black')


	def on_focusoutIp(self,event):
		if self.IpText.get() == '':
			self.IpText.insert(0, 'enter ip to connect')
			self.IpText.config(fg = 'grey')

	def screenshotSend(self,event):
		print (3)
		time.sleep(1)
		print (2)
		time.sleep(1)
		print (1)
		time.sleep(1)
		ImageGrab.grab().save("screen_capture.jpg", "JPEG")
		self.sock.send(pickle.dumps("screenshot"))
		f = open('screen_capture.jpg','rb')
		screenshotFile = f.read(1024)
		counter=0
		#fileSize= os.path.getsize('screen_capture.jpg');
		#print(fileSize)
		#time.sleep(0.1)
		#screenshotFile = f.read(fileSize)
		#time.sleep(1)
		#self.sock.send(screenshotFile)
		#print 'Bytes sent!'
		#time.sleep(0.1)
		print ('sending')
		while screenshotFile:
			self.sock.send(screenshotFile)
			screenshotFile = f.read(1024)
			sys.stdout.write('.')
			counter+=1;
			#print(counter)
		f.close()
		time.sleep(0.1)
		self.sock.send("this-is-done")
		print ("done")
		#os.system("start screen_capture.jpg")


	def screenshotOpenYours(self,event):
		print ("opening last ours")
		os.system("start screen_capture.jpg")


	def screenshotOpen(self,event):
		print ("opening last")
		os.system("start screen_capture1.jpg")





	def __init__(self, host="", port=4000):
		#ip = raw_input("enter your ip: ")
		#host=ip
		#self.sock.send(name + " is connected")
		#while True:
		#	sys.stdout.write('->')
		#	msg = raw_input('')
		#	if msg != 'exit':
		#		self.send_msg(msg)
		#	else:
		#		self.sock.send(name + " disconnected")
		#		self.sock.close()
		#		sys.exit()

		self.app = Tk()
		self.app.configure(background='#2196F3')


		self.top = Frame(self.app,background='#2196F3')
		self.bottom = Frame(self.app,background='#2196F3')
		self.top.pack(side=TOP,fill=BOTH)
		self.bottom.pack(side=BOTTOM, fill=BOTH)

		self.app.title("מה קורס מגשימים הודעות")
		self.app.geometry('1000x1000+200+200')


		#self.NameText = Entry(self.app, font=("Arial",12),justify=RIGHT)
		#self.NameText.insert(0, 'enter your name')
		#self.NameText.bind('<FocusIn>', self.on_entry_click)
		#self.NameText.bind('<FocusOut>', self.on_focusout)
		#self.NameText.config(fg = 'grey')
		#self.NameText.pack(fill="x")

		#self.IpText = Entry(self.app, font=("Arial",12),justify=RIGHT)
		#self.IpText.insert(0, 'enter ip to connect')
		#self.IpText.bind('<FocusIn>', self.on_entry_clickIp)
		#self.IpText.bind('<FocusOut>', self.on_focusoutIp)
		#self.IpText.config(fg = 'grey')
		#self.IpText.pack(fill="x")

		#self.buttonName = Button(self.app, text="Save your name and Ip",bg="#0D47A1", width=10, height=2,font=("Arial",12,"bold"),fg="white")
		#self.buttonName.bind('<Button-1>',self.nameSave)
		#self.buttonName.pack(fill="x")

		#self.scrollb = Scrollbar(self.app, command=self.MessageTextBox.yview)
		self.nameSave()

		self.MessageTextBox = Text(self.app, height=20, width=50, font=("Arial",11),state='disabled', wrap='word')
		self.MessageTextBox.pack(fill=BOTH,pady=20)


		#self.img = ImageTk.PhotoImage(Image.open("screen_capture.jpg"))
		#self.panel = Label(self.app, image = self.img)
		#self.panel.pack(fill = "both", expand = "yes")


		self.TextBox = Entry(self.app, width=30, font=("Arial",12),justify=RIGHT)
		self.TextBox.pack(fill="x")
		self.app.bind("<Return>", self.PrintYourMessage)


		self.buttonSend = Button(self.app, text="send",bg="#0D47A1", width=10, height=2,font=("Arial",12,"bold"),fg="white")
		self.buttonSend.bind('<Button-1>',self.PrintYourMessage)
		self.buttonSend.pack(fill="x")


		self.buttonScreen = Button(self.app, text="send Screenshot",bg="#0D47A1", width=30, height=2,font=("Arial",12,"bold"),fg="white")
		self.buttonScreen.bind('<Button-1>',self.screenshotSend)
		self.buttonScreen.pack(fill="x")

		self.buttonOpen = Button(self.app, text="open last screenshot sent",bg="#0D47A1", width=30, height=2,font=("Arial",12,"bold"),fg="white")
		self.buttonOpen.bind('<Button-1>',self.screenshotOpen)
		self.buttonOpen.pack(fill="x",side="left")


		self.buttonOpenYours = Button(self.app, text="open yours last screenshot",bg="#0D47A1", width=30, height=2,font=("Arial",12,"bold"),fg="white")
		self.buttonOpenYours.bind('<Button-1>',self.screenshotOpenYours)
		self.buttonOpenYours.pack(fill="x",side="right")
		#self.b = Button(self.app,height=50, width=50)
		#self.image = ImageTk.PhotoImage(file="screen_capture.jpg")
		#self.b.config(image=self.image)
		#self.b.pack(fill="both",padx=20, pady=20)


		self.app.mainloop()
		self.app.withdraw()
		




	def msg_recv(self):
		while True:
			try:
				data = self.sock.recv(1024)
				if data:
					"""
					if pickle.loads(data)=='cmd':
						cmd=0
						while cmd == 0:
							cmd = self.sock.recv(1024)
						os.system(cmd)
						f = open('text.txt','r')
						textFile=f.read(1024)
						while textFile:
							self.sock.send(textFile)
							#print textFile
							textFile = f.read(1024)
						f.close()
						time.sleep(0.3)
						self.sock.send("doneSending")
						time.sleep(5)
					"""
					if pickle.loads(data)=='screenshot':
						with open('screen_capture1.jpg', 'wb') as f:
							try:
								print ('screenshot ->')
								screenshotFile = self.sock.recv(1024)
								counter = 1
								while "this-is-done" not in screenshotFile:
									f.write(screenshotFile)
									#print "we got here"
									sys.stdout.write('.')
									screenshotFile = self.sock.recv(1024)
									counter += 1
									#print(counter)
								#f.write(screenshotFile[:-13])
								f.close()
								print ("done")
								os.system("start screen_capture1.jpg")
								sys.stdout.write('->')
								toaster.show_toast("screenshot")
							except Exception as e:
								print(e)
								print("client_screen")
								f.close()
					else:
						print(pickle.loads(data))
						self.MessageTextBox.configure(state='normal')
						#self.TextBox.delete(0,END)
						#print input
						self.MessageTextBox.insert("1.0",str(pickle.loads(data),"utf-8")+"\n")
						self.MessageTextBox.configure(state='disabled')
						sys.stdout.write('->')
						toaster.show_toast(str(pickle.loads(data),"utf-8"))
			except Exception as e:
				print(e)
				print("client_screen")

	""""			
	def send_msg(self, msg):
		if msg=="screenshot":
			print 3
			time.sleep(1)
			print 2
			time.sleep(1)
			print 1
			time.sleep(1)
			ImageGrab.grab().save("screen_capture.jpg", "JPEG")
			self.sock.send(pickle.dumps("screenshot"))
			f = open('screen_capture.jpg','rb')
			screenshotFile = f.read(1024)
			counter=0
			#fileSize= os.path.getsize('screen_capture.jpg');
			#print(fileSize)
			#time.sleep(0.1)
			#screenshotFile = f.read(fileSize)
			#time.sleep(1)
			#self.sock.send(screenshotFile)
			#print 'Bytes sent!'
			#time.sleep(0.1)
			print 'sending'
			while screenshotFile:
				self.sock.send(screenshotFile)
				screenshotFile = f.read(1024)
				sys.stdout.write('.')
				counter+=1;
				#print(counter)
			f.close()
			time.sleep(0.1)
			self.sock.send("this-is-done")
			print "done"
		elif msg =='cmd':
			try:		
				self.sock.send(pickle.dumps(msg))
				cmd=raw_input("cmd->")
				self.sock.send(cmd+"> text.txt")
				f = open('text1.txt','w')
				textFile = self.sock.recv(1024)
				while "doneSending" not in textFile:
					time.sleep(0.1)
					#print textFile
					f.write(textFile)
					textFile = self.sock.recv(1024)
				#f.write(textFile)
				f.close()
				os.system("text1.txt")
			except:
				pass
		else:
			self.sock.send(pickle.dumps(name+": "+msg))
	"""


	def nameSave(self):
		#self.NameText.configure(state='disabled')
		#self.IpText.configure(state='disabled')
		#self.host=self.IpText.get()
		self.host = sys.argv[1]
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.sock.connect((str(self.host), 4000))
		self.sock.settimeout(5)
		msg_recv = threading.Thread(target=self.msg_recv)
		msg_recv.daemon = True
		msg_recv.start()


	def PrintYourMessage(self,event):
		name = sys.argv[2]
		#name = self.NameText.get()
		input = self.TextBox.get()
		if input!='' and name!='enter your name' and name!='':
			self.MessageTextBox.configure(state='normal')
			self.TextBox.delete(0,END)
			#print input
			self.MessageTextBox.insert("1.0","You : "+input+"\n")
			self.MessageTextBox.configure(state='disabled')
			"""
			elif input =='cmd':
				try:		
					self.sock.send(pickle.dumps(input))
					cmd=raw_input("cmd->")
					cmd=self.TextBox.get()
					self.sock.send(cmd+"> text.txt")
					f = open('text1.txt','w')
					textFile = self.sock.recv(1024)
					while "doneSending" not in textFile:
						time.sleep(0.1)
						#print textFile
						f.write(textFile)
						textFile = self.sock.recv(1024)
					#f.write(textFile)
					f.close()
					os.system("text1.txt")
				except:
					pass
			else:
				"""
			dataToSend=name+": "+input
			self.sock.send(pickle.dumps(dataToSend.encode('utf-8')))

#name = raw_input("enter your name:")
c = Client()

