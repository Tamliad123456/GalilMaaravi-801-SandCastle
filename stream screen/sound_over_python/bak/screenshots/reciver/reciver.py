import os
import matplotlib.pyplot as pl
import time
from PIL import ImageGrab
from threading import Thread
import socket
import base64

count = 0
def get_names():
	filelist=os.listdir('C:\\Users\\magshimim\\Documents\\screenshots\\reciver')
	for fichier in filelist:
		if not(fichier.endswith(".png")):
			filelist.remove(fichier)
	return filelist

s = socket.socket()
s.bind(('0.0.0.0',1337))
s.listen(1)
(clientsocket, address) = s.accept()


def show_images(filelist):
	img = None
	print(filelist)
	for f in filelist:
		try:
			print(f)
			im=pl.imread(f)
			if img is None:
				img = pl.imshow(im)
			else:
				img.set_data(im)
			pl.pause(.01)
			pl.draw()
			os.remove(f)
		except Exception as e:
			print(e)


def get_files():
	while True:
		global clientsocket
		global count
		data = clientsocket.recv(99999999)
		data = base64.b64decode(data, altchars=None)
		if data:
			f = open("file_recived" + str(count) + ".png","wb")
			f.write(data)
			f.close()
			count += 1


t1 = Thread(target = get_files)
t1.start()
while True:
	names = get_names()
	show_images(names)
