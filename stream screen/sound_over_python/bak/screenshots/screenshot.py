import os
import matplotlib.pyplot as pl
import time
from PIL import ImageGrab
from threading import Thread
import socket

def create_screenshot():
	for f in range(20):
		f = str(f) + ".png"
		ImageGrab.grab().save(f, "PNG")


def get_names():
	filelist=os.listdir('C:\\Users\\magshimim\\Documents\\screenshots')
	for fichier in filelist:
		if not(fichier.endswith(".png")):
			filelist.remove(fichier)
	return filelist

def send_screenshot(msg):
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	s.sendto(msg, ('127.0.0.1',1337))

def show_images(filelist):
	img = None
	for f in filelist:
		print(f)
		im=pl.imread(f)
		if img is None:
			img = pl.imshow(im)
		else:
			img.set_data(im)
		pl.pause(.01)
		pl.draw()
		os.remove(f)

while True:
	t1 = Thread(target = create_screenshot)
	if not t1.isAlive():
		t1.start()
	names = get_names()
	try:
		show_images(names)
	except Exception as e:
		print(e)
