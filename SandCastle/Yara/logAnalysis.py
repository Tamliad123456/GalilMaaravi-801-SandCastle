import os

def getLogYaraRules():
    return os.popen("yara.exe logAnalysis.yar dynamic.log").read()

def analyseYaraRules(data):
    context = {"level" : 0, "malicious_activity" :[]}
    if "credential_stealing" in data:
        context["level"] = 2
        context["malicious_activity"].append("credential_stealing")
    if "process_holowing_or_doupleganging" in data:
        context["level"] = 2
        context["malicious_activity"].append("process_holowing_or_doupleganging")
    if "could_be_ransomware" in data:
        if context["level"] < 1:
            context["level"] = 1
        context["malicious_activity"].append("could_be_ransomware")
    if "psexec" in data:
        context["level"] = 2
        context["malicious_activity"].append("psexec")
    if "os_files_manipulations" in data:
        context["level"] = 2
        context["malicious_activity"].append("os_files_manipulations")
    if "ransom_or_scanner" in data:
        if context["level"] < 1:
            context["level"] = 1
        context["malicious_activity"].append("ransom_or_scanner")
    if "powershell" or "cmd" in data:
        context["level"] = 2
        context["malicious_activity"].append("command_execution")
    if "kernelCheck" in data:
    	context["level"] = 2
    	context["malicious_activity"].append("kernel manipulation")
    return context


def main():
    data = getLogYaraRules()
    ans = analyseYaraRules(data)

    if ans["level"] == 1:
        print("Process could be Malicious")
    elif ans["level"] == 2:
        print("Process is Malicious")
    elif ans["level"] == 0:
        print("Process is not Malicious")


main()