#the main script to check the file for bad behavior

import os
import time
from subprocess import check_output
import psutil
import subprocess


dll_list = [
r"C:\Windows\SYSTEM32\ntdll.dll",
r"C:\Windows\System32\KERNEL32.DLL",
r"C:\Windows\System32\KERNELBASE.dll",
r"C:\Windows\SYSTEM32\apphelp.dll",
r"C:\Windows\System32\ADVAPI32.dll",
r"C:\Windows\System32\msvcrt.dll",
r"C:\Windows\System32\sechost.dll",
r"C:\Windows\System32\RPCRT4.dll",
r"C:\Windows\System32\CRYPT32.dll",
r"C:\Windows\System32\ucrtbase.dll",
r"C:\Windows\System32\MSASN1.dll",
r"C:\Windows\System32\ole32.dll",
r"C:\Windows\System32\combase.dll",
r"C:\Windows\System32\bcryptPrimitives.dll",
r"C:\Windows\System32\GDI32.dll",
r"C:\Windows\System32\gdi32full.dll",
r"C:\Windows\System32\msvcp_win.dll",
r"C:\Windows\System32\USER32.dll",
r"C:\Windows\System32\win32u.dll",
r"C:\Windows\System32\OLEAUT32.dll",
r"C:\Windows\System32\SHLWAPI.dll",
r"C:\Windows\System32\SHELL32.dll",
r"C:\Windows\System32\cfgmgr32.dll",
r"C:\Windows\System32\shcore.dll",
r"C:\Windows\System32\windows.storage.dll",
r"C:\Windows\System32\profapi.dll",
r"C:\Windows\System32\powrprof.dll",
r"C:\Windows\System32\kernel.appcore.dll",
r"C:\Windows\System32\cryptsp.dll",
r"C:\Windows\System32\SETUPAPI.dll",
r"C:\Windows\System32\bcrypt.dll",
r"C:\Windows\System32\WLDAP32.dll",
r"C:\Windows\SYSTEM32\Cabinet.dll",
r"C:\Windows\SYSTEM32\cryptdll.dll",
r"C:\Windows\SYSTEM32\FLTLIB.DLL",
r"C:\Windows\SYSTEM32\NETAPI32.dll",
r"C:\Windows\SYSTEM32\SAMLIB.dll",
r"C:\Windows\SYSTEM32\Secur32.dll",
r"C:\Windows\SYSTEM32\USERENV.dll",
r"C:\Windows\SYSTEM32\VERSION.dll",
r"C:\Windows\SYSTEM32\HID.DLL",
r"C:\Windows\SYSTEM32\WinSCard.dll",
r"C:\Windows\SYSTEM32\WINSTA.dll",
r"C:\Windows\SYSTEM32\DEVOBJ.dll",
r"C:\Windows\SYSTEM32\CRYPTBASE.DLL",
r"C:\Windows\SYSTEM32\SRVCLI.DLL",
r"C:\Windows\SYSTEM32\LOGONCLI.DLL",
r"C:\Windows\SYSTEM32\NETUTILS.DLL",
r"C:\Windows\SYSTEM32\WKSCLI.DLL",
r"C:\Windows\SYSTEM32\SSPICLI.DLL",
r"C:\Windows\System32\IMM32.DLL",
r"C:\Windows\SYSTEM32\rsaenh.DLL",
r"C:\Windows\SYSTEM32\vaultcli.DLL",
r"C:\Windows\SYSTEM32\wintypes.dll",
r"C:\Windows\System32\WINTRUST.dll"

]

def run_check_script(extention, run_order, ip):
    '''function to distribute the task of checking the file'''

    #os.system("changeSystemTime.exe")
    
    if(run_order == 0):
        errors = run_static(extention)
        return (errors if errors != "NO ERRORS" else run_dynamic(extention, ip))
    elif run_order == 1:
        return run_static(extention)
    elif run_order == 2:
        return run_dynamic(extention, ip)

    return None


def run_static(extention):
    '''function to run the static (yara) analysis on the given file'''

    os.system(r"..\Yara\yara64.exe ..\Yara\rules.yar to_run." + extention + " > yar_output.txt")

    with open("yar_output.txt", 'r') as yar:
        if "to_run" not in yar.readlines():
            return "NO ERRORS"
        return [x for x in yar.readlines() if "to_run" in x]

    os.system(r"..\Yara\yara64.exe ..\Yara\Suspicios.yar to_run." + extention + " > yar_output.txt")

    with open("yar_output.txt", 'r') as yar:
        if "to_run" not in yar.readlines():
            return "NO ERRORS"
        return [x for x in yar.readlines() if "to_run" in x]
    
    return None


def run_dynamic(extention, ip):
    '''function to run the dynamic (with hooks) analysis on the given file'''
    pid = ''
    sniff = subprocess.Popen(["python", "sniffer.py ", ip])
    #print("python sniffer.py", ip)
    os.system("start StartSuspended.exe")
    #os.system("start to_run." + extention)
    #pid = int(check_output(["pidof", "to_run." + extention]))
    path = os.getcwd().replace("\\", "\\\\") + r"\\to_run.exe"
    #print(path)
    while pid == '' or pid == ' ' or type(pid) == type(list()):
        try:
            pid = (os.popen(r"wmic process where ExecutablePath='" + path + r"' get ProcessId").read().split("\n"))
            pid = int(pid[2].replace(" ", ""))
        except:
            pass
    #p = psutil.Process(pid)
    #p.suspend()

    #os.system(r"..\injector\DllInjector.exe " + str(pid) + r" HookDll.dll")
    #print("BEFORE LISTDLLS")
    dlls = os.popen("listdlls to_run.exe").read()
    #p.resume()
    #os.system(r"wnfdump.exe -n WNF_AUDC_CAPTURE > audioCaptureWNF.txt")
    proc = subprocess.Popen(['cmd.exe', '/c', 'wnfdump.exe', '-n', "WNF_AUDC_CAPTURE", '>', 'audioCaptureWNF.txt'], shell=True)
    mimikatz = False
    counter = 0
    for x in dlls:
        if x in dll_list:
            counter += 1

    if len(dll_list) * 0.75 <= counter:
        mimikatz = True
    #time.sleep(1*30)
    #proc.terminate()
    #print("BEFORE PSUTIL PID")
    #print(pid)
    while psutil.pid_exists(pid):
        pass
    os.system("taskkill /f /im wnfdump.exe")
    sniff.kill()
    proc.kill()

    if mimikatz:
        with open("dynamic.log", "a") as log_file:
            log_file.write("mimikatz found!!")
    subprocess.call(("python", "parser.py"))
