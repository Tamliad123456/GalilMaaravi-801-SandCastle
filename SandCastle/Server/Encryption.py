from base64 import urlsafe_b64encode, urlsafe_b64decode
from Crypto.Cipher import AES
from Crypto import Random


BS = 16
pad = lambda s: s + (BS - len(s) % BS) * chr(BS - len(s) % BS)
unpad = lambda s: s[:-ord(s[len(s) - 1:])]

base64pad = lambda s: s + '=' * (4 - len(s) % 4)
base64unpad = lambda s: s.rstrip("=")
encrypt_key = 'LKHlhb899Y09olUi'

#def padEncoded(enc):
#    for _ in range(0, 16):
#        if len(enc) % BS != 0:
#            enc = enc.decode()
#            enc += chr(0)
#            enc = enc.encode()
#        else:
#            return enc

def encrypt(key, msg):
    iv = Random.new().read(BS)
    cipher = AES.new(key.encode(), AES.MODE_CBC, iv)
    #print(pad(str(msg)), len(pad(str(msg))))
    #print(pad(str(msg)).encode(), len(pad(str(msg)).encode()))
    encrypted_msg = cipher.encrypt(pad(str(msg)).encode())
    return base64unpad(urlsafe_b64encode(iv + encrypted_msg).decode())


# when incorrect encryption key is used, `decrypt` will return empty string
def decrypt(key, msg):
    decoded_msg = urlsafe_b64decode(base64pad(msg.decode()))
    iv = decoded_msg[:BS]
    encrypted_msg = decoded_msg[BS:] 
    cipher = AES.new(key.encode(), AES.MODE_CBC, iv)
    return unpad(cipher.decrypt(encrypted_msg))

