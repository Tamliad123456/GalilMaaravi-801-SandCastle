#this class makes a file hash and checks if the file has already been run on the server
import os, base64, hashlib, binascii, sqlite3

def get_file_base64(filename):
    '''function to get the base of a given file'''
    
    if os.path.isfile(filename):
        with open(filename, "rb") as the_file:
            data = the_file.read()
            the_file.close()
    else:
        raise Exception("File doesnt exists")

    return base64.b64encode(data).decode()


def hash_file(b64_code):
    '''function to hash a given base64 code in pbkdf2 hash with salt'''

    fh = hashlib.pbkdf2_hmac('sha256', bytes(b64_code), b'find_way_to_hide_salt', 1337)
    output =  binascii.hexlify(fh)
    print(output)

    return output

def get_table_columns(table):
    '''function to get all of the columns of the given table'''

    database = sqlite3.connect("file_hashes.db")
    connection = database.cursor()
    connection.execute("PRAGMA table_info(" + table + ");")

    output = connection.fetchall()
    connection.commit()
    connection.close()
    columns = tuple()

    for val in output:
        columns += (val.split('|')[1],)#adding values to the tuple by the return syntax of 0|col_name|TYPE|1|0

    return str(columns)

def insert_to_db(table, ordered_keys):
    '''function to insert a row into the given table with the given values'''

    database = sqlite3.connect("file_hashes.db")
    connection = database.cursor()
    connection.execute("INSERT INTO " + table + " VALUES"+ str(ordered_keys))

    output = connection.fetchone()
    connection.commit()
    connection.close()
    if output != None:
        print(output)
        print("An error might have occured")


def check_for_hash_match(file_hash):
    '''function to check if the given hash is in a database.
    for now a sqlite database is assumed - subject to change'''

    database = sqlite3.connect("file_hashes.db")
    connection = database.cursor()
    connection.execute("SELECT * FROM files WHERE hash=?", file_hash)

    result = connection.fetchone()
    connection.commit()
    connection.close()

    if(result == None):
        print("File has never been run before. Clear to Run!")
        return True

    print("File was already checked! You shouldn't run it again")
    return False