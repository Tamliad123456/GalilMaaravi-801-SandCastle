import socket
import base64
import json
import os
import logging
import check_file as checker
import random
import Encryption
import hashlib
import threading
import data_usage as usage
import time


BUFFER_SIZE = 4096
IP = "0.0.0.0"
PORT = 1337
LOG_FILE = "dynamic.log"
NET_LOG = "NetLog.log"

usage_value = []
usage_stop = False

run_type = 0

logging.basicConfig(level=logging.DEBUG)

extentions = {"exe" : "1"}#, "dll" : "2", "bat" : "3", "vbs" : "4", "com" : "5", "pyc" : "6", "py" : "7", "else" : "8"}
commands = {"send_file" : "1", "stop_run" : "2", "send_log" : "3", "disconnect" : "4", "le_supriso" : "1337"}
params = {"proceed_to_dynamic" : "1", "force_dynamic" : "2", "skip_static" : "3", "limit_to_static" : "4"}

current_extention = ""

ip = ""

def main():
    '''the main function'''
    global ip
    
    t = threading.Thread(target=os.system, args=("python dothestream.py",))
    t.start()

    
    with socket.socket() as sock:
        sock.bind((IP, PORT))
        sock.listen(0)

        while True:
            client, addr = sock.accept()
            t2 = threading.Thread(target=os.system, args=("StreamScreenClient.exe",))
            t2.start()
            Encryption.encrypt_key = exchange_keys(client)
            #print(Encryption.encrypt_key)
            logging.info("accepted " + str(addr))
            ip = addr[0]
            handle_client(client, addr)
            client.close()
            os.system("taskkill /f /im StreamScreenClient.exe")

    #send_log()

    sock.close()


def send_data(sock, data):
    data = Encryption.encrypt(Encryption.encrypt_key, data)
    sock.sendall(data.encode())
    print("Data Sent")

def recv_data(sock):
    data = sock.recv(1024)
    data = Encryption.decrypt(Encryption.encrypt_key, data)
    print("Received Data")
    return data.decode()

def exchange_keys(sock):
    p = 43579
    g = int(sock.recv(1024))
    a1 = random.randint(1,4500)
    a = (g**a1) % p
    #print(str(a))
    sock.send(str(a).encode())
    a2 = int(sock.recv(1024))

    m = hashlib.sha256()
    m.update(str((a2**a1) % p).encode())
    return m.hexdigest()[:16]

def handle_client(client, addr):
    '''the function is for handeling the client messages'''
    message = ""
    while "Finished" not in message:
        message += client.recv(1024).decode()
    message = message[:-8]
    message = Encryption.decrypt(Encryption.encrypt_key, message.encode()).decode()
    #print(message)
    message = json.loads(message)
    recv_file(message, client) 

    #while True:
    #    message = recv_data(client)
    #    print(message)
    #    logging.info(message)
    #    message = json.loads(message)
    #    
    #    if message["command"] == commands["stop_run"]:
    #        stop_run(client)
    #        break
    #    elif message["command"] == commands["le_supriso"]:
    #        #placeholder for easteregg
    #        pass
    #    else:
    #        logging.error("Error! wrong command! how did you do this?")

    disconnect(client)



def disconnect(sock):
    '''the function is for sending a disconnection message'''
    try:
        message = {}
        data = "Session has ended. You might want to connect again"
        command = commands["disconnect"]

        message["data"] = data
        message["command"] = command

        send_data(sock, json.dumps(message))
    except:
        pass

def recv_file(message, sock=None):
    '''the function is for reciving a new file to run'''
    data = message["data"]

    data = base64.b64decode(data)
    if message["extention"] == "else":
        message["extention"] = "txt"
    try:
    	run_type = int(message["run_type"])
    except:
    	pass
    #print(message["extention"])
    with open("to_run." + find_extention(message["extention"]), "wb") as to_run:    
        to_run.write(data)
    
    if message["extention"] != "txt":
        run_file_as_admin("to_run", find_extention(message["extention"]), sock)


def find_extention(num):
    for x in extentions.keys():
        if extentions[x] == num:
            return x

def run_file_as_admin(filename, extention, sock=None):
    #ctypes.windll.shell32.ShellExecuteW(None, "runas", sys.executable, filename, None, 1)
    global current_extention, ip, finished, usage_stop
    logging.debug(extention + " " + filename)
    current_extention = extention

    usage_thread = threading.Thread(target=get_usage, args=("to_run.exe",))
    usage_thread.start()
    #print("the ip is:" + ip)
    result = checker.run_check_script(extention, run_type, ip)
    usage_stop = True
    #if result != "":
        #print(result)

    stop_run(sock)

def send_log(sock):
    data = ""
    try:
        while data == "":
            with open(LOG_FILE, "r") as log_file:
                data = log_file.read()
    except Exception as e:
        #print(e)
        pass
    try:
        with open(NET_LOG, "r") as net_log:
            data += "\n **NETWORK_LOG**" + net_log.read()
    except Exception as e:
        #print(e)
        pass
    
    for x in usage_value:
        for y in range(7):
            if y == 0:
                data += "ram usage: " + str(x[y]) + ", "
            elif y == 1:
                data += "cpu usage: " + str(x[y]) + ", "
            elif y == 2:
                data += "disk usage: " + str(x[y]) + ", "
            elif y == 3:
                data += "process cpu usage: " + str(x[y]) + ", "
            elif y == 4:
                data += "process ram usage: " + str(x[y]) + ", "
            elif y == 5:
                data += "process disk usage: " + str(x[y]) + ", "
            elif y == 6:
                data += "is valid value: " + str(x[y]) + '\n'

    message = {"command" : commands["send_log"], "data" : data}
    send_data(sock, json.dumps(message))
    sock.send(str("Finished").encode())
    sock.close()

def stop_run(sock):
    os.system("taskkill /f /im to_run." + current_extention)
    send_log(sock)


def get_usage(path):
    counter = 0
    while not usage_stop:
        try:
            pid = (os.popen(r"wmic process where ExecutablePath='" + os.getcwd().replace('\\', '\\\\') + '\\\\' + path + r"' get ProcessId").read().split("\n"))
            pid = int(pid[2].replace(" ", ""))
            usage_value.append([usage.get_ram_precentage(), usage.get_cpu_precentage(), usage.get_disk_percentage(), usage.get_process_cpu(pid), usage.get_process_ram(pid), usage.get_process_disk(pid), usage.is_valid(pid)])
            time.sleep(1)
        except Exception as e:
            pass


if __name__ == "__main__":
    main()
