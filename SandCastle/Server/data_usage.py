import psutil

def get_ram_precentage():
    return psutil.virtual_memory()[2]

def get_cpu_precentage():
    return psutil.cpu_percent()

def get_disk_percentage():
	return psutil.disk_usage("C:\\\\")[3]

def get_process_cpu(pid):
    return psutil.Process(pid).cpu_percent()

def get_process_ram(pid):
    return psutil.Process(pid).memory_percent()

def get_process_disk(pid):
	return psutil.Process(pid).io_counters()[1]

def is_valid(pid):
    return get_process_cpu(pid) < 95 and get_process_ram(pid) < 95 and get_process_disk(pid) < 20000
