import pbkdf2
import sys
import os

if len(sys.argv) > 1:
    if os.path.exists(sys.argv[1]):
        with open(sys.argv[1], "rb") as binary_file:
            data = binary_file.read()
            print(pbkdf2.SHA1(data).hexdigest())