from scapy.all import wrpcap, sniff, IP
import logging
import sys

logging.basicConfig(level=logging.DEBUG)

def save_packets():
	global pkts
	wrpcap("packets.pcap", pkts)

def pkt_callback(pkt):
	logging.info(pkt.summary())

def func(pkt):
	if IP in pkt and pkt[IP].dst != my_ip and pkt[IP].src != my_ip:
		return True
	return False


my_ip = sys.argv[1]

pkts = sniff(prn=pkt_callback, lfilter=func)
print(type(pkts))
save_packets()