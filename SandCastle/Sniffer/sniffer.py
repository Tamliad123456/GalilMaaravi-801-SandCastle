from scapy.all import wrpcap, sniff
import logging

packets = {}

logging.basicConfig(level=logging.DEBUG)

def save_packets():
	global pkts
	wrpcap("packets.pcap", pkts)

def pkt_callback(pkt):
	logging.info(pkt.summary())


pkts = sniff(prn=pkt_callback, store=1000, count=1000)
save_packets()