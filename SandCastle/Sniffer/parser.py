from scapy.all import IP, Raw, rdpcap, TCP, UDP, Ether, ICMP
import logging, os, requests
#import bytes


logging.basicConfig(level=logging.DEBUG, filename='NetLog.log')

def load_packets():
    packets = rdpcap('packets.pcap')
    return packets

def find_my_ip():
    fullmsg = Ether() / IP() / ICMP()
    return fullmsg[IP].src

def find_ips(pckts):
    address = []
    for packet in pckts:
        #suspicious.json
        if IP in packet:
            if Raw in packet:
                logging.info(packet[IP].src + " ---- " + packet[IP].dst + " The data = " + str(packet[Raw]) );                    
            else:
                logging.info(packet[IP].src + " ---- " + packet[IP].dst + " Without data" )
            if packet[IP].src == find_my_ip() and packet[IP].dst not in address:
                address.append(packet[IP].dst)
            elif packet[IP].src not in address:
                address.append(packet[IP].src)

    url = "http://api.apility.net/badip/"
    header = {'X-Auth-Token' : '079e58b1-4b64-410b-ba3f-7448ac6e2fb2'}

    for ip in address:
        #response = requests.get(url + ip, header)
        #if response.status_code == 200:
        #    logging.info("malicious IP: " + ip)
        pass
    malicious_ip(address)


def malicious_ip(ip_address):

    block = [
    "http://opendbl.net/lists/etknown.list",
    "http://opendbl.net/lists/tor-exit.list",
    "http://opendbl.net/lists/bruteforce.list",
    "http://opendbl.net/lists/blocklistde-all.list",   
    "http://opendbl.net/lists/talos.list",
    "http://opendbl.net/lists/dshield.list",
    "http://opendbl.net/lists/malwaredomain.list",
    "http://opendbl.net/lists/sslblock.list",
    "http://opendbl.net/lists/zeustracker.list",
    ]

    malicious_str = ""

    for site in block:
        r = requests.get(site)
        malicious_str += r.text
    print(malicious_str)

    for ip in ip_address:
        if ip in malicious_str:
            print(ip + " is malicious")
        else:
            print(ip + " not malicious")
    #curl -H @{'X-Auth-Token' = '079e58b1-4b64-410b-ba3f-7448ac6e2fb2'} http://api.apility.net/badip/

def main():
    packets = load_packets()

    TCP_count = 0
    UDP_count = 0
    for pkt in packets:
        if TCP in pkt:
            TCP_count += 1
        
        if UDP in pkt:
            UDP_count += 1

    logging.debug(TCP_count)
    logging.debug(UDP_count)

    find_ips(packets)
if __name__ == "__main__":
    main()