import sys 
import datetime 

def main():
    '''the main function'''
    if len(sys.argv) < 4:
        print("""wrong params:
1)file name
2)file hash
3)explenation
4)log file""")
    else:
        file_name = sys.argv[1]
        file_hash = sys.argv[2]
        explenation = sys.argv[3]
        log_file = sys.argv[4]
        with open("template.html", "r") as t:
            template_data = t.read()
            template_data = template_data.replace("{{FILE_NAME}}", file_name)
            template_data = template_data.replace("{{HASH}}", file_hash)
            template_data = template_data.replace("{{SHORT_EXPLENATION}}", explenation)
            template_data = template_data.replace('name="log_file">', 'name="log_file">' + open(log_file, "r").read())
            template_data = template_data.replace("{{TIME}}", str(datetime.datetime.now()))
            with open(file_name + "-log.html", "w") as f:
                f.write(template_data)

main()

