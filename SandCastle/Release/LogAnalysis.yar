rule credential_stealing
{
    strings:
        $lsass = "lsass.exe"
        $mimikatz = "mimikatz"
        $mimikatz2 = "mimikatz.exe"
    condition:
        any of them
}


rule process_holowing_or_doupleganging
{
    strings:
        $open_process = "opened process "
        $suspend = "on suspend"

    condition:
        $open_process and $suspend
}


rule could_be_ransomware
{
    strings:
        $openFile = "has been opened"
        $writeFile = "write to file"

    condition:
        #openFile > 50 and #writeFile > 50
}


rule psexec
{
    strings:
        $process_param = "opened with params psexec"
        $process_name = "The process psexec has been opened"
    
    condition:
        any of them
}

rule cmd
{
    strings:
        $process_param = "opened with params cmd.exe"
        $process_param2 = "opened with params cmd"
        $process_name = "The process cmd.exe has been opened"
    
    condition:
        any of them
}

rule powershell
{
    strings:
        $process_param = "opened with params powershell.exe"
        $process_param2 = "opened with params powershell"
        $process_name = "The process powershell.exe has been opened"
    
    condition:
        any of them
}


rule os_files_manipulations
{
    strings: 
        $write_os_files = /Writing to file \\\?\?\\C:\\Windows.*[$\\]\w+/ nocase
        
    condition:
        any of them
}

rule ransom_or_scanner
{
    strings:
        $read_all_data = /Reading from file \\\?\?\\C:\\Users.*[$\\]\w+/ nocase
        $write_all_data = /Writing to file \\\?\?\\C:\\Users.*[$\\]\w+/ nocase
    condition:
        (#read_all_data > 100) or ((#read_all_data == #write_all_data) and (#write_all_data > 100))//if reads A LOT of files or reads and writes the same amount that is very big
}

rule kernelCheck
{
	strings:
		$kernel = "kernel"
		$Kernel = "Kernel"
	condition:
		(any of them) and (not cmd)
}